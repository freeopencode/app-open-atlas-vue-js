import mongoose from 'mongoose'
import dotenv from 'dotenv'

// Connexion à la base de données

// La variable d'environnement MONGO_URL contient le lien pour se connecter à la database
dotenv.config({ path: '../../../backend/.env' })

// Récupérer l'URL de la base de données à partir des variables d'environnement
const mongoURL = process.env.MONGO_URL

// Connexion à la base de données MongoDB
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('Connexion réussie à la base de données MongoDB')
  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données MongoDB', error)
  })
