import express from 'express'
import router from './routes/routes.js'
// import { config } from './config.js'
// import mongoose from 'mongoose'
import cors from 'cors'
import epciGeojsonController from './controllers/epcisGeojsonController.js'
// import { getEpciGeoJSON } from './controllers/epcisGeojsonController.js'
import { getCasudGeoJSON } from './controllers/casudController.js'
import { getCinorGeoJSON } from './controllers/cinorController.js'
import { getCirestGeoJSON } from './controllers/cirestController.js'
import { getCivisGeoJSON } from './controllers/civisController.js'
import { getTcoGeoJSON } from './controllers/tcoController.js'
import { getCommunesGeoJSON } from './controllers/communesGeojsonController.js'
import path from 'path'

// Initialisation et configuration

const app = express()
const port = 3000

// Middleware pour analyser le corps des requêtes JSON
app.use(express.json())

// Configurer CORS pour autoriser les requêtes du frontend
const corsOptions = {
  origin: 'http://localhost:8081',
  optionsSuccessStatus: 200
}

// Utilisation du middleware CORS
app.use(cors(corsOptions))

// Utiliser toutes les routes définies dans le fichier de routes central
app.use('/', router)

app.use(epciGeojsonController)

// Middleware pour servir les fichiers statiques depuis le dossier 'views'
const currentModulePath = new URL(import.meta.url).pathname
app.use(express.static(path.join(path.dirname(currentModulePath), 'views')))

// Servir le fichier GeoJSON
// app.get('/mapepci', (req, res) => {
//   // Charger le fichier GeoJSON et l'envoyer à la réponse
//   const geoJSONData = getEpciGeoJSON
//   res.send(geoJSONData)
// })

app.get('/casud', (req, res) => {
  // Charger le fichier GeoJSON et l'envoyer à la réponse
  const geoJSONData = getCasudGeoJSON
  res.send(geoJSONData)
})

app.get('/cinor', (req, res) => {
  // Charger le fichier GeoJSON et l'envoyer à la réponse
  const geoJSONData = getCinorGeoJSON
  res.send(geoJSONData)
})

app.get('/cirest', (req, res) => {
  // Charger le fichier GeoJSON et l'envoyer à la réponse
  const geoJSONData = getCirestGeoJSON
  res.send(geoJSONData)
})

app.get('/civis', (req, res) => {
  // Charger le fichier GeoJSON et l'envoyer à la réponse
  const geoJSONData = getCivisGeoJSON
  res.send(geoJSONData)
})

app.get('/tco', (req, res) => {
  // Charger le fichier GeoJSON et l'envoyer à la réponse
  const geoJSONData = getTcoGeoJSON
  res.send(geoJSONData)
})

app.get('/mapcommunes', (req, res) => {
  // Charger le fichier GeoJSON et l'envoyer à la réponse
  const geoJSONData = getCommunesGeoJSON
  res.send(geoJSONData)
})

// Ecoutez sur le port spécifié
app.listen(port, () => {
  console.log(`Le serveur Node JS fonctionne sur http://localhost:${port} `)
})

export default app

// const mongoURL = config.mongoURL

// Connexion à la base de données MongoDB
// mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
//   .then(async () => {
//     console.log('Connexion réussie à la base de données MongoDB')
//   })
//   .catch((error) => {
//     console.error('Erreur de connexion à la base de données MongoDB', error)
//   })
