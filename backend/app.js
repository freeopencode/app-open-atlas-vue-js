// app.js

// Importation des modules
import express from 'express'
import mongoose from 'mongoose'
// import csrf from 'csurf'
// import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import { fileURLToPath } from 'url'
import path from 'path'
import chalk from 'chalk'
import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import session from 'express-session'
import bcrypt from 'bcrypt'
import cors from 'cors'
import 'dotenv/config'
// import connectMongo from 'connect-mongo'
import connectMongoDBSession from 'connect-mongodb-session'
import flash from 'connect-flash'

// Importation des modèles
import Commune from './models/MongoDB/Commune.js'
import Country from './models/MongoDB/Country.js'
import Epci from './models/MongoDB/Epci.js'
import Subject from './models/MongoDB/Subject.js'
import epciModel from './models/epcigeojson.js'
import communeModel from './models/communesgeojson.js'
import User from './models/MongoDB/User.js'

// Importation des middlewares
import ensureAuthenticated from './middleware/authMiddleware.js'

// Importation des routes
import countriesRoutes from './routes/countries.js'
import epcisRoutes from './routes/epcis.js'
import communesRoutes from './routes/communes.js'
import subjectsRoutes from './routes/subjects.js'

// Configuration
const port = process.env.PORT || 3000
const dbUrl = process.env.DB_URL
const app = express()

// Configuartion de mongoose
// Configuration de mongoose
mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true })

// Configurer CORS pour autoriser les requêtes du frontend
const corsOptions = {
  origin: 'http://localhost:8081',
  optionsSuccessStatus: 200
}

// Utilisation du middleware CORS
app.use(cors(corsOptions))

// Configuration de session et passport
// const MongoStore = connectMongo(session)
const MongoDBStore = connectMongoDBSession(session)
const store = new MongoDBStore({
  uri: dbUrl,
  collectionName: 'sessions',
  expires: 1000 * 60 * 60 * 24 * 7
})

store.on('error', function (error) {
  console.error(error)
})

// Utiliser cookie-parser avant csurf pour gérer les cookies
// app.use(cookieParser)

app.use(session({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: true,
  // Permet d'utiliser la même connexion à la base de données que Mongoose
  // store: new MongoStore({ mongooseConnection: mongoose.connection })
  store
}))

// Protection contre les attaques CSRF
// const csrfProtection = csrf({ cookie: { httpOnly: true, secure: true } })
// app.use(csrfProtection)

// Gestion des erreurs CSRF
// app.use((err, req, res, next) => {
//   if (err.code !== 'EBADCSRFTOKEN') return next(err)
//   // Erreur CSRF
//   res.status(403).send('Erreur CSRF détectée')
// })

// Utiliser connect-flash
app.use(flash())

// Configuration de Passport
passport.use(new LocalStrategy(
  async function (username, password, done) {
    try {
      // Trouver l'utilisateur par le nom de l'utilisateur
      const user = await User.findOne({ username })

      // Si l'utilisateur n'est pas trouvé
      if (!user) {
        return done(null, false, { message: 'Nom d\'utilisateur ou mot de passe incorrect' })
      }

      // Si le mot de passe ne correspond pas
      if (!user.comparePassword || !user.comparePassword(password)) {
        return done(null, false, { message: 'Nom d\'ulisateur ou mot de passe incorrect' })
      }
      // Si l'utilisateur et le mot de passe sont valides
      return done(null, user)
    } catch (error) {
      return done(error)
    }
  }
))

passport.serializeUser(function (user, done) {
  done(null, user.id)
})

passport.deserializeUser(async function (id, done) {
  try {
    const user = await User.findById(id)
    done(null, user)
  } catch (error) {
    done(error)
  }
})
app.use(passport.initialize())
app.use(passport.session())

// Configuration du moteur de templates EJS
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
console.log('Chemin vers l\' application :', __dirname)
console.log('Chemin vers la view en cours :', __dirname)
app.set('views', path.join(__dirname, 'views', 'ejs'))
app.set('view engine', 'ejs') // Utilise EJS comme moteur de template

// Middleware pour les fichiers statiques
// Pour afficher le CSS
app.use('/css', express.static(path.join(__dirname, 'views/css')))
// Pour afficher les assets (images, etc)
app.use('/assets', express.static(path.join(__dirname, 'views/assets')))

// Middleware pour les données POST
app.use(bodyParser.urlencoded({ extended: true }))

// Middleware pour passer les messages flash à toutes les vues
app.use((req, res, next) => {
  res.locals.successMessages = req.flash('success')
  res.locals.errorMessages = req.flash('error')
  next()
})

// Routes principales

// Route vers la page d'accueil
// app.get('/', (req, res) => {
//   const user = req.user
//   res.render('home', { user })
// })

app.get('/', (req, res) => {
  // Vérifier si l'utilisateur est connecté
  if (req.isAuthenticated()) {
    // Utilisateur connecté, rediriger vers la page d'accueil (dashboard)
    return res.redirect('/dashboard')
  } else {
    // Utilisateur non connecté, rediriger vers la page de connexion
    return res.redirect('/login')
  }
})

// Routes pour afficher les données

app.get('/view-countries', async (req, res) => {
  const countries = await Country.find()
  res.render('countries', {
    countries,
    user: req.user
    // csrfToken: req.csrfToken() // Pour passer le jeton CSRF
  })
})

app.get('/view-epcis', async (req, res) => {
  const epcis = await Epci.find()
  res.render('epcis', {
    epcis,
    user: req.user
  })
})

app.get('/view-communes', async (req, res) => {
  const communes = await Commune.find()
  res.render('communes', {
    communes,
    user: req.user
  })
})

app.get('/view-subjects', async (req, res) => {
  const subjects = await Subject.find()
  res.render('subjects', {
    subjects,
    user: req.user
  })
})

app.get('/epciGeojsonView', async (req, res) => {
  const geoJSONData = await epciModel.getAllGeoJSONData()
  res.status(200).send(geoJSONData)
})

app.get('/communeGeojsonView', async (req, res) => {
  const geoJSONData = await communeModel.getAllGeoJSONData()
  res.status(200).send(geoJSONData)
})

// Routes pour les données GeoJSON

app.get('/epciGeojsonData', async (req, res) => {
  const geoJSONData = await epciModel.getAllGeoJSONData()
  // Autoriser l'accès depuis n'importe quelle origine
  res.header('Access-Control-Allow-Origin', '*')
  // Autoriser les méthodes GET
  res.header('Access-Control-Allow-Methods', 'GET')
  // Autoriser les en-têtes spécifiés
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  res.setHeader('Content-Type', 'application/json')
  res.send(geoJSONData)
})

app.get('/communeGeojsonData', async (req, res) => {
  const geoJSONData = await communeModel.getAllGeoJSONData()
  // Autoriser l'accès depuis n'importe quelle origine
  res.header('Access-Control-Allow-Origin', '*')
  // Autoriser les méthodes GET
  res.header('Access-Control-Allow-Methods', 'GET')
  // Autoriser les en-têtes spécifiés
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  res.setHeader('Content-Type', 'application/json')
  res.send(geoJSONData)
})

// Routes pour gérer les modèles (CRUD)

// Utiliser les routes des pays
app.use(countriesRoutes)

// Utiliser les routes des epcis
app.use(epcisRoutes)

// Utiliser les routes des communes
app.use(communesRoutes)

// Utiliser les routes des sujets
app.use(subjectsRoutes)

// Routes pour l'authentification

// Route vers la page d'enregistrement de l'utilisateur
app.get('/register', (req, res) => {
  res.render('register')
})

// Ajouter un nouvel utilisateur
app.post('/register', async (req, res) => {
  try {
    const { username, email, password } = req.body
    // Vérifier si l'utilisateur existe déjà
    const existingUser = await User.findOne({ username })
    if (existingUser) {
      console.log('L\'utilisateur existe déjà: ', existingUser.username)
      return res.redirect('/register') // Rediriger vers la page d'inscription
    }

    // Hash du mot de passe avant de l'enregistrer dans la base de données
    const hashedPassword = await bcrypt.hash(password, 10)

    // Création d'un nouvel utilisateur
    const newUser = new User({
      username,
      email,
      password: hashedPassword
    })

    // Enregistrer le nouvel utilisateur dans la base de données
    await newUser.save()
    console.log('Nouvel utilisateur enregistré :', newUser.username)
    res.redirect('/login') // Rediriger l'utilisateur vers la page login
  } catch (error) {
    // Gestion des erreurs
    console.error('Erreur lors de l\'enregistrement de l\'utilisateur:', error)
    res.redirect('/register') // Rediriger vers la page d'inscription
  }
})

// Route vers la page de connexion
app.get('/login', (req, res) => {
  res.render('login')
})

// Route vers le login (authentification)
app.post('/login', passport.authenticate('local', {
  successRedirect: '/dashboard',
  failureRedirect: '/login',
  failureFlash: true
}))

// Route sécurisée
app.get('/dashboard', ensureAuthenticated, (req, res) => {
  // Code de gestion de la route sécurisée
  const user = req.user
  res.render('dashboard', { user })
})

// Route pour se déconnecter
app.get('/logout', (req, res, next) => {
  req.logout((err) => {
    if (err) {
      return next(err)
    }
    res.redirect('/')// Redirige l'utilisateur vers la page d'accueil aprés la déconnexion
  })
})

// Gestion des erreurs 404
app.use((req, res) => {
  res.status(404).render('404')
})

// Démarrage du serveur
app.listen(port, () => {
  console.log('Le backoffice fonctionne sur ' + chalk.blue(`http://localhost:${port} `))
})
