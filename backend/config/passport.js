import passport from 'passport'
import LocalStrategy from 'passport-local.Strategy'
import bcrypt from 'bcrypt'
import User from '../models/MongoDB/User'

passport.use(new LocalStrategy(
  async (username, password, done) => {
    try {
      // Logique de récupération de l'utilisateur
      const user = await User.findOne({ username })

      if (!user) {
        return done(null, false, { message: 'Nom d\'utilisateur incorrect' })
      }
      const passwordMatch = await bcrypt.compare(password, user.password)

      if (!passwordMatch) {
        return done(null, false, { message: 'Mot de passe incorrect.' })
      }

      return done(null, user)
    } catch (error) {
      return done(error)
    }
  }
))

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser(async (id, done) => {
  try {
    // Logique de récupération de l'utilisateur
    const user = await User.findById(id)
    done(null, user)
  } catch (error) {
    done(error)
  }
})
