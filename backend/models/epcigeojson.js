// Model : epcigeojson.js

import fs from 'fs/promises'
import path from 'path'
import { fileURLToPath } from 'url'

// Chemin vers le fichier GeoJSON
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const geoJSONPath = path.join(__dirname, '../models/epci.geojson')

// Fonction pour récupérer les données GeoJSON
async function getAllGeoJSONData () {
  try {
    const geoJSONContent = await fs.readFile(geoJSONPath, 'utf-8')
    return JSON.parse(geoJSONContent)
  } catch (error) {
    console.error('Erreur de lecture du fichier JSON', error)
    return null
  }
}

export default {
  getAllGeoJSONData
}
