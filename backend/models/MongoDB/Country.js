// Model Country.js

import mongoose from 'mongoose'

const countrySchema = new mongoose.Schema({
  id: {
    type: Number,
    required: [true, 'L\'ID du sujet est requis.'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'Le nom du pays est requis.']
  },
  codeInsee: Number,
  timestamp: Date
})

const Country = mongoose.model('Country', countrySchema)

export default Country
