// Epci.js
import mongoose from 'mongoose'
// import AutoIncrementFactory from 'mongoose-sequence'

const epciSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: [true, 'L\'ID du sujet est requis.'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'Le nom de l\'EPCI est requis.']
  },
  codeSIREN: Number,
  country: String,
  timestamp: Date
})

// Ajouter le plugin autoIncrement au schéma
// epciSchema.plugin(AutoIncrementFactory(mongoose), { inc_field: 'id' })

const Epci = mongoose.model('Epci', epciSchema)

export default Epci
