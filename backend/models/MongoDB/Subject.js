// Subject.js
import mongoose from 'mongoose'

const subjectSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: [true, 'L\'ID du sujet est requis.'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'Le nom du sujet est requis.']
  },
  theme: {
    type: String,
    required: [true, 'Le nom du thème est requis.']
  },
  scale: {
    type: String,
    required: [true, 'Le nom du territoire est requis.']
  },
  timestamp: Date
})

const Subject = mongoose.model('Subject', subjectSchema)

export default Subject
