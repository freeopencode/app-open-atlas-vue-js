// Quartier.js
import mongoose from 'mongoose'
// import AutoIncrementFactory from 'mongoose-sequence'

const quartierSchema = new mongoose.Schema({
  // L'id n'est plus spécifié manuellement
  name: {
    type: String,
    required: [true, 'Le nom du quartier est requis.']
  },
  codeInsee: Number,
  commune: {
    id: Number,
    name: String
  },
  epci: {
    id: Number,
    name: String
  },
  timestamp: Date
})

// Ajouter le plugin autoIncrement au schéma
// quartierSchema.plugin(AutoIncrementFactory(mongoose), { inc_field: 'id' })

const Quartier = mongoose.model('Quartier', quartierSchema)

export default Quartier
