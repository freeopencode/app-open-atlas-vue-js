// Commune.js

import mongoose from 'mongoose'

const communeSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: [true, 'L\'ID du sujet est requis.'],
    unique: true
  },
  name: {
    type: String,
    required: [true, 'Le nom de la commune est requis.']
  },
  codeInsee: Number,
  country: String,
  epci: String,
  timestamp: Date
})

const Commune = mongoose.model('Commune', communeSchema)

export default Commune
