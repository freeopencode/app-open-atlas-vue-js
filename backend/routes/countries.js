// routes/countries.js
import express from 'express'
import ensureAuthenticated from '../middleware/authMiddleware.js'
import Country from '../models/MongoDB/Country.js'

const router = express.Router()

// READ - Pays

router.get('/countries', ensureAuthenticated, async (req, res) => {
  const countries = await Country.find()
  res.render('countries', { countries, user: req.user })
})

// CREATE - Affiche le formulaire d'ajout / d'édition
router.get('/countries/:id/edit', async (req, res) => {
  const country = await Country.findById(req.params.id)
  res.render('countries_edit', { country })
})

// UPDATE - Traitement du formulaire d'ajout / édition
router.post('/countries', async (req, res) => {
  // Vérifier le jeton CSRF
  // const csrfToken = req.body._csrf || req.query._csrf || req.headers['x-csrf-token']
  // if (!csrfToken || csrfToken !== req.csrfToken()) {
  //   return res.status(403).send('Jetons CSRF invalides')
  // }
  // Déclaration de la variable message avec une chaîne de caractères
  // let message = ''

  // Avant la sauvegarde
  console.log('Avant la sauvegarde du pays : ', req.body)

  try {
    if (req.body._id) {
      // Si l'ID est présent, mise à jour du pays existante
      await Country.findByIdAndUpdate(req.body._id, req.body)
      // message = 'Le pays a été mis à jour avec succès.'
    } else {
      // Sinon créer un nouveau pays
      const newCountry = new Country(req.body)
      await newCountry.save()
      // message = 'Le pays a été créé avec succès'
    }

    // Ajouter un message de console.log pour afficher les données dans la console
    console.log('Données saisies avec succès :', req.body)
  } catch (error) {
    // Gestion des erreurs
    console.error('Erreur lors de la saisie des données : ', error.message)
    res.status(500).send('Erreur lors de la sauvegarde des données')
    return // Sortir de la fonction en cas d'erreur
  }
  // Récupérer la liste des pays
  const countries = await Country.find()
  res.render('countries', { countries })
})

// DELETE - Supprime un pays
router.get('/countries/:id/delete', async (req, res) => {
  // Supprimer le pays
  await Country.findByIdAndDelete(req.params.id)

  // Récupérer la liste des pays après la suppression
  const countries = await Country.find()
  // Rendre la page avec la liste mise à jour et la variable user
  res.render('countries', { countries, user: req.user })
})

export default router
