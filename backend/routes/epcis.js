// routes/countries.js
import express from 'express'
import ensureAuthenticated from '../middleware/authMiddleware.js'
import Epci from '../models/MongoDB/Epci.js'

const router = express.Router()

// READ - Epci

router.get('/epcis', ensureAuthenticated, async (req, res) => {
  const epcis = await Epci.find()
  res.render('epcis', { epcis, user: req.user })
})

// CREATE - Affiche le formulaire d'ajout / d'édition
router.get('/epcis/:id/edit', async (req, res) => {
  const epci = await Epci.findById(req.params.id)
  res.render('epcis_edit', { epci })
})

// UPDATE - Traitement du formulaire d'ajout / édition
router.post('/epcis', async (req, res) => {
  // Avant la sauvegarde
  console.log('Avant la sauvegarde de l\'EPCI: ', req.body)

  try {
    if (req.body._id) {
      // Si l'ID est présent, mise à jour de la commune existante
      await Epci.findByIdAndUpdate(req.body._id, req.body)
    } else {
      // Sinon créer une nouvelle EPCI
      const newEpci = new Epci(req.body)
      await newEpci.save()
    }
    // Ajouter un message de console.log pour afficher les données dans la console
    console.log('Données saisies avec succès :', req.body)
  } catch (error) {
    // Gestion des erreurs
    console.error('Erreur lors de la saisie des données : ', error.message)
    res.status(500).send('Erreur lors de la sauvegarde des données')
    return // Sortir de la fonction en cas d'erreur
  }
  // Rendre la page avec le message
  // Récupérer la liste des pays
  const epcis = await Epci.find()
  res.render('epcis', { epcis })
})

// DELETE - Supprime une epci
router.get('/epcis/:id/delete', async (req, res) => {
  // Supprimer l'EPCI
  await Epci.findByIdAndDelete(req.params.id)

  // Récupérer la liste des epcis après la suppression
  const epcis = await Epci.find()
  // Rendre la page avec la liste mise à jour et la variable user
  res.redirect('epcis', { epcis, user: req.user })
})

export default router
