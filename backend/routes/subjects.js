// routes/subjects.js
import express from 'express'
import ensureAuthenticated from '../middleware/authMiddleware.js'
import Subject from '../models/MongoDB/Subject.js'

const router = express.Router()

// READ - Sujets

router.get('/subjects', ensureAuthenticated, async (req, res) => {
  const subjects = await Subject.find()
  res.render('subjects', { subjects })
})

// CREATE - Affiche le formulaire d'ajout / d'édition
router.get('/subjects/:id/edit', async (req, res) => {
  const subject = await Subject.findById(req.params.id)
  res.render('subjects_edit', { subject })
})

// UPDATE - Traitement du formulaire d'ajout / édition
router.post('/subjects', async (req, res) => {
  // Avant la sauvegarde
  console.log('Avant la sauvegarde du sujet : ', req.body)

  try {
    if (req.body._id) {
      // Si l'ID est présent, mise à jour de la commune existante
      await Subject.findByIdAndUpdate(req.body._id, req.body)
    } else {
      // Sinon créer un nouveau sujet
      const newSubject = new Subject(req.body)
      await newSubject.save()
    }
    // Ajouter un message de console.log pour afficher les données dans la console
    console.log('Données saisies avec succès :', req.body)
  } catch (error) {
    // Gestion des erreurs
    console.error('Erreur lors de la saisie des données : ', error.message)
    res.status(500).send('Erreur lors de la sauvegarde des données')
    return // Sortir de la fonction en cas d'erreur
  }
  // Récupérer la liste des sujets
  const subjects = await Subject.find()
  res.render('subjects', { subjects })
})

// DELETE - Supprime un sujet
router.get('/subjects/:id/delete', async (req, res) => {
  await Subject.findByIdAndDelete(req.params.id)
  res.redirect('/subjects')
})

export default router
