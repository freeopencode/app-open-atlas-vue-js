// routes/countries.js
import express from 'express'
import ensureAuthenticated from '../middleware/authMiddleware.js'
import Commune from '../models/MongoDB/Commune.js'

const router = express.Router()

// READ - Communes

router.get('/communes', ensureAuthenticated, async (req, res) => {
  const communes = await Commune.find()
  res.render('communes', { communes, user: req.user })
})

// CREATE - Affiche le formulaire d'ajout / d'édition
router.get('/communes/:id/edit', async (req, res) => {
  const commune = await Commune.findById(req.params.id)
  res.render('communes_edit', { commune })
})

// UPDATE - Traitement du formulaire d'ajout / édition
router.post('/communes', async (req, res) => {
  // Avant la sauvegarde
  console.log('Avant la sauvegarde de la commune: ', req.body)
  try {
    if (req.body._id) {
      // Si l'ID est présent, mise à jour de la commune existante
      await Commune.findByIdAndUpdate(req.body._id, req.body)
    } else {
      // Sinon créer une nouvelle commune
      const newCommune = new Commune(req.body)
      await newCommune.save()
    }
    // Ajouter un message de console.log pour afficher les données dans la console
    console.log('Données saisies avec succès :', req.body)
    res.redirect('/communes')
  } catch (error) {
    // Gestion des erreurs
    console.error('Erreur lors de la saisie des données : ', error.message)
    res.status(500).send('Erreur lors de la sauvegarde des données')
    return // Sortir de la fonction en cas d'erreur
  }
  // Récupérer la liste des communes
  const communes = await Commune.find()
  res.render('communes', { communes })
})

// DELETE - Supprime une commune
router.get('/communes/:id/delete', async (req, res) => {
  // Supprimer la commune
  await Commune.findByIdAndDelete(req.params.id)
  // Récupérer la liste des communes après la suppression
  const communes = await Commune.find()
  // Rendre la page avec la liste mise à jour et la variable user
  res.redirect('communes', { communes, user: req.user })
})

export default router
