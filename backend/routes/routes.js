// import http from 'http'
import express from 'express'
import epcisController from '../controllers/epcisController.js'
import communesController from '../controllers/communesController.js'
import { getEpciGeoJSON } from '../controllers/epcisGeojsonController.js'
import { getCinorGeoJSON } from '../controllers/cinorController.js'
import { getCirestGeoJSON } from '../controllers/cirestController.js'
import { getTcoGeoJSON } from '../controllers/tcoController.js'
import { getCasudGeoJSON } from '../controllers/casudController.js'
import { getCivisGeoJSON } from '../controllers/civisController.js'
import { getCommunesGeoJSON } from '../controllers/communesGeojsonController.js'

const router = express()

// router.get('/', 'Hello from Node JS !')

// Définir les routes
router.get('/epcis', epcisController.getAllEpcis)
router.get('/communes', communesController.getAllCommunes)

// Routes pour les fichiers GeoJSON
router.get('/mapepci', getEpciGeoJSON)
router.get('/mapcommunes', getCommunesGeoJSON)
router.get('/casud', getCasudGeoJSON)
router.get('/cinor', getCinorGeoJSON)
router.get('/cirest', getCirestGeoJSON)
router.get('/civis', getCivisGeoJSON)
router.get('/tco', getTcoGeoJSON)

// const host = 'localhost'
// const port = 8000

// router.listen(port, () => {
//   console.log(`Le router fonctionne sur http://${host}:${port}`)
// })

export default router
