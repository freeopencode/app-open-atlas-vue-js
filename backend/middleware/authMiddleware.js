// Sécurisation des routes qui nécessitent une authentification
function ensureAuthenticated (req, res, next) {
  if (req.isAuthenticated()) {
    return next() // Authentifié, passe à la route suivante
  }
  res.redirect('/login') // Non authentifié, redirige vers la page de connexion
}

export default ensureAuthenticated
