import casudModel from '../models/casud.js'

// Contrôleur pour envoyer le fichier GeoJSON
export const getCasudGeoJSON = async (req, res) => {
  const geoJSONData = await casudModel.getAllGeoJSONData()
  if (geoJSONData) {
    res.setHeader('Content-Type', 'application/json')
    res.send(geoJSONData)
  } else {
    res.status(500).send('Internal Server Error')
  }
}
