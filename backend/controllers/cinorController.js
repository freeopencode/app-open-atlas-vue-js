import cinorModel from '../models/cinor.js'

// Contrôleur pour envoyer le fichier GeoJSON
export const getCinorGeoJSON = async (req, res) => {
  const geoJSONData = await cinorModel.getAllGeoJSONData()
  if (geoJSONData) {
    res.setHeader('Content-Type', 'application/json')
    res.send(geoJSONData)
  } else {
    res.status(500).send('Internal Server Error')
  }
}
