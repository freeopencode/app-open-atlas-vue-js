import mongoose from 'mongoose'
import dotenv from 'dotenv'

// Connexion à la base de données

// La variable d'environnement MONGO_URL contient le lien pour se connecter à la database
dotenv.config({ path: '../../../backend/.env' })

// Récupérer l'URL de la base de données à partir des variables d'environnement
const mongoURL = process.env.MONGO_URL

// Connexion à la base de données MongoDB
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('Connexion réussie à la base de données MongoDB')
    // Interactions avec la base de données
    const db = mongoose.connection
    // Je crée une collection (équivalent de la table en SQL)

    // J'insére les EPCIs
    const collectionEPCIs = db.collection('EPCIs')
    const insertEPCIs = await collectionEPCIs.insertMany([
      { id: 1, name: 'CASUD', codeSIREN: 249740085, timestamp: new Date() },
      { id: 2, name: 'CINOR', codeSIREN: 249740119, timestamp: new Date() },
      { id: 3, name: 'CIREST', codeSIREN: 249740119, timestamp: new Date() },
      { id: 4, name: 'CIVIS', codeSIREN: 249740077, timestamp: new Date() },
      { id: 5, name: 'TCO', codeSIREN: 249740101, timestamp: new Date() }
    ])
    // Je vérifie si tout s'est bien passé
    console.log(`Documents insérés => ${insertEPCIs} `)
  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données MongoDB', error)
  })
