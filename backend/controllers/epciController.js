import Epci from '../model/Epci.js'

export const getEpcis = async (req, res) => {
  try {
    // Récupérer tous les EPCIs
    const epcis = await Epci.find()
    res.json(epcis)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des EPCIs' })
  }
}

export const updateEpci = async (req, res) => {
  const { id } = req.params
  const { name, codeSIREN } = req.body

  try {
    const updatedEpci = await Epci.findByIdAndUpdate(id, { name, codeSIREN }, { new: true })

    if (!updatedEpci) {
      return res.status(404).json({ error: 'EPCI non trouvé' })
    }

    res.json(updatedEpci)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'EPCI' })
  }
}

export const deleteEpci = async (req, res) => {
  const { id } = req.params
  try {
    const deletedEpci = await Epci.findByIdAndRemove(id)

    if (!deletedEpci) {
      return res.status(400).json({ error: 'EPCI non trouvé' })
    }

    res.json({ message: 'EPCI supprimé avec succès' })
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'EPCI' })
  }
}
