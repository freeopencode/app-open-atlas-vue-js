// Controller : communesGeojsonController.js

import communeModel from '../models/communesgeojson.js'
import fs from 'fs/promises'
import { fileURLToPath } from 'url'
import path from 'path'
import express from 'express'

// Chemin vers le fichier HTML de vue
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
console.log('Chemin du contrôleur:', __dirname)
const viewPath = path.join(__dirname, '..', 'views', 'ejs', 'communeGeojsonView.ejs')

const router = express.Router()

// Nouvelle route pour servir le fichier HTML
router.get('/communeGeojsonView', async (req, res) => {
  try {
    console.log('Route /communeGeojsonView atteinte')
    const viewContent = await fs.readFile(viewPath, 'utf-8')
    res.setHeader('Content-Type', 'text/html')
    res.send(viewContent)
  } catch (error) {
    console.error('Erreur de lecture du fichier HTML de vue', error)
    res.status(500).send('Internal Server Error')
  }
})

// Route pour servir le fichier GeoJSON
router.get('/communeGeojsonData', async (req, res) => {
  console.log('Route /epciGeojsonData atteinte')
  const geoJSONData = await communeModel.getAllGeoJSONData()
  res.setHeader('Content-Type', 'application/json')
  res.send(geoJSONData)
})

// Contrôleur pour envoyer le fichier GeoJSON
export const getEpciGeoJSON = async (req, res) => {
  const geoJSONData = await communeModel.getAllGeoJSONData()

  // Si la demande spécifie le format HTML, renvoyer la vue HTML
  if (req.headers.accept.includes('text/html')) {
    try {
      const viewContent = await fs.readFile(viewPath, 'utf-8')

      const htmlWithData = viewContent.replace('communes.geojson', JSON.stringify(geoJSONData, null, 2))
      res.setHeader('Content-Type', 'text/html')
      res.send(htmlWithData)
    } catch (error) {
      console.error('Erreur de lecture du fichier HTML de vue', error)
      res.status(500).send('Internal Server Error')
    }
  } else {
    res.setHeader('Content-Type', 'application/json')
    res.send(geoJSONData)
  }
}

export default router
