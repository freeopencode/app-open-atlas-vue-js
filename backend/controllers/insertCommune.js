import mongoose from 'mongoose'
import dotenv from 'dotenv'

// Connexion à la base de données

// La variable d'environnement MONGO_URL contient le lien pour se connecter à la database
dotenv.config({ path: '../../../backend/.env' })

// Récupérer l'URL de la base de données à partir des variables d'environnement

const mongoURL = process.env.MONGO_URL
// Connexion à la base de données MongoDB
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('Connexion réussie à la base de données MongoDB')
    // Interactions avec la base de données
    const db = mongoose.connection
    // Je crée une collection (équivalent de la table en SQL)

    // J'insére les Communes
    const collectionCommunes = db.collection('Communes')
    const insertCommunes = await collectionCommunes.insertMany([
    // Les coordonnées geopoint sont en WGS84
      { id: 1, name: 'Les Avirons', codeInsee: 97401, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 2, name: 'Bras Panon', codeInsee: 97402, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 3, name: 'Entre-Deux', codeInsee: 97403, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 4, name: 'Etang-Salé', codeInsee: 97404, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 5, name: 'Petite-Ile', codeInsee: 97405, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 6, name: 'Plaine des Palmistes', codeInsee: 97406, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 7, name: 'Le Port', codeInsee: 97407, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 8, name: 'La Possession', codeInsee: 97408, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 9, name: 'Saint-André', codeInsee: 97409, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 10, name: 'Saint-Benoît', codeInsee: 97410, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 11, name: 'Saint-Denis', codeInsee: 97411, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 12, name: 'Saint-Joseph', codeInsee: 97412, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 13, name: 'Saint-Leu', codeInsee: 97413, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 14, name: 'Saint-Louis', codeInsee: 97414, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 15, name: 'Saint-Paul', codeInsee: 97415, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 16, name: 'Saint-Pierre', codeInsee: 97416, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 17, name: 'Saint-Philippe', codeInsee: 97417, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 18, name: 'Sainte-Marie', codeInsee: 97418, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 19, name: 'Sainte-Rose', codeInsee: 97419, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 20, name: 'Sainte-Suzanne', codeInsee: 97420, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 21, name: 'Salazie', codeInsee: 97421, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 22, name: 'Le Tampon', codeInsee: 97422, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 23, name: 'Trois-Bassins', codeInsee: 97423, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 24, name: 'Cilaos', codeInsee: 97424, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() }
    ])

    // Je vérifie si tout s'est bien passé
    console.log(`Documents insérés =>  ${insertCommunes} `)
  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données MongoDB', error)
  })
