import Quartier from '../model/Quartier.js'

export const getQuartiers = async (req, res) => {
  try {
    // Récupérer tous les quartiers
    const quartiers = await Quartier.find()
    res.json(quartiers)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des quartiers' })
  }
}

export const updateQuartier = async (req, res) => {
  const { id } = req.params
  const { name, codeInsee, commune, epci } = req.body

  try {
    const updates = { name, codeInsee, commune, epci }
    const updatedQuartier = await Quartier.updateQuartier(id, updates)

    if (!updatedQuartier) {
      return res.status(404).json({ error: 'Quartier non trouvé' })
    }

    res.json(updatedQuartier)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour du quartier' })
  }
}

export const deleteQuartier = async (req, res) => {
  const { id } = req.params
  try {
    const deletedQuartier = await Quartier.findByIdAndRemove(id)

    if (!deletedQuartier) {
      return res.status(400).json({ error: 'Quartier non trouvé' })
    }

    res.json({ message: 'Quartier supprimé avec succès' })
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression du quartier' })
  }
}
