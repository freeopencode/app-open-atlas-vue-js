// communesController.js
import Commune from './models/MongoDB/Commune.js'

// Fonction pour gérer la demande d'obtention de toutes les données Communes
const getAllCommunes = async (req, res) => {
  try {
    const communes = await Commune.find()
    res.json(communes)
  } catch (error) {
    console.error('Erreur lors de la récupération des communes : ', error)
    res.status(500).json({ error: 'Erreur lors de la récupération des communes' })
  }
}

// Afficher une commune par Id

const getCommuneById = async (req, res) => {
  try {
    const commune = await Commune.findById(req.params.id)
    if (!commune) {
      return res.status(404).json({ error: 'Commune non trouvée' })
    }
    res.json(commune)
  } catch (error) {
    console.error('Erreur lors de la récupération de la commune :', error)
    res.status(500).json({ error: 'Erreur lors de la récupération de la commune' })
  }
}

// Affiche le formulaire d'édition
const getEditCommuneForm = async (req, res) => {
  try {
    const commune = await Commune.findById(req.params.id)
    if (!commune) {
      return res.status(404).json({ error: 'Commune non trouvée' })
    }
    res.render('communes_edit', { commune })
  } catch (error) {
    console.error('Erreur lors de la récupération de la commune :', error)
    res.status(500).json({ error: 'Erreur lors de la récupération de la commune' })
  }
}

// Enregistre ou met à jour une commune
const saveOrUpdateCommune = async (req, res) => {
  const communeData = req.body
  try {
    if (communeData._id) {
      // Mise à our de la commune existante
      await Commune.findByIdAndUpdate(communeData._id, communeData)
    } else {
      // Création d'une nouvelle commune
      const newCommune = new Commune(communeData)
      await newCommune.save()
    }
    console.log('Données saisies avec succès :', communeData)
    res.redirect('/communes')
  } catch (error) {
    console.error('Erreur lors de la sauvegarde des données :', error.message)
    res.status(500).send('Erreur lors de la sauvegarde des données')
  }
}

// Supprime une commune

// Exportez la fonction
export default {
  getAllCommunes,
  getCommuneById,
  getEditCommuneForm,
  saveOrUpdateCommune
}
