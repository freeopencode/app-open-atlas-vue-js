import cirestModel from '../models/cirest.js'

// Contrôleur pour envoyer le fichier GeoJSON
export const getCirestGeoJSON = async (req, res) => {
  const geoJSONData = await cirestModel.getAllGeoJSONData()
  if (geoJSONData) {
    res.setHeader('Content-Type', 'application/json')
    res.send(geoJSONData)
  } else {
    res.status(500).send('Internal Server Error')
  }
}
