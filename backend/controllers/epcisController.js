import epcisModel from '../models/epcis.js'

// Fonction pour gérer la demande d'obtention de toutes les données EPCIs
function getAllEpcis (req, res) {
  const epcisData = epcisModel.getAllEpcis()

  // Définir l'en-tête de type de contenu
  res.setHeader('Content-Type', 'application/json')

  // Envoyer le code 200 si la connexion a réussi
  res.writeHead(200)

  // Envoyer les données JSON avec une indentation pour améliorer la lisibilité
  res.end(epcisData, null, 2)
  // res.json(epcisData)
}

// Exportez la fonction
export default {
  getAllEpcis
}
