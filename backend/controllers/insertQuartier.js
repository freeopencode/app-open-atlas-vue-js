import mongoose from 'mongoose'
import dotenv from 'dotenv'

// Connexion à la base de données

// La variable d'environnement MONGO_URL contient le lien pour se connecter à la database
dotenv.config({ path: '../../../backend/.env' })

// Récupérer l'URL de la base de données à partir des variables d'environnement
const mongoURL = process.env.MONGO_URL

// Connexion à la base de données MongoDB
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('Connexion réussie à la base de données MongoDB')
    // Interactions avec la base de données
    const db = mongoose.connection
    // Je crée une collection (équivalent de la table en SQL)

    // J'insére les Quartiers (grand quartier INSEE)
    const collectionQuartiers = db.collection('Quartiers')
    const insertQuartiers = await collectionQuartiers.insertMany([
      // Les Avirons
      { id: 101, name: 'Les Avirons', codeInsee: 9740101, commune: { id: 1, name: 'Les Avirons' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 102, name: 'Tévelave', codeInsee: 9740102, commune: { id: 1, name: 'Les Avirons' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      // Bras Panon
      { id: 201, name: 'Bras Panon', codeInsee: 9740201, commune: { id: 2, name: 'Bras Panon' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      // Entre Deux
      { id: 301, name: 'Entre-Deux', codeInsee: 9740301, commune: { id: 3, name: 'Entre-Deux' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      // Etang Salé
      { id: 401, name: 'Centre-ville', codeInsee: 9740401, commune: { id: 4, name: 'Etang-Salé' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 402, name: 'Les Canots', codeInsee: 9740402, commune: { id: 4, name: 'Etang-Salé' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 403, name: 'Etang Salé Les Bains', codeInsee: 9740403, commune: { id: 4, name: 'Etang-Salé' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 404, name: 'Le Maniron', codeInsee: 9740404, commune: { id: 4, name: 'Etang-Salé' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 405, name: 'La Ravine Sèche', codeInsee: 9740405, commune: { id: 4, name: 'Etang-Salé' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      // Petite Ile
      { id: 501, name: 'Centre-ville', codeInsee: 9740501, commune: { id: 5, name: 'Petite Ile' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 502, name: 'Piton Goyaves', codeInsee: 9740502, commune: { id: 5, name: 'Petite Ile' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      // Plaine des Palmistes
      { id: 601, name: 'Plaine des Palmistes', codeInsee: 9740601, commune: { id: 6, name: 'Plaine des Palmistes' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      // Le Port
      { id: 701, name: 'Centre-ville', codeInsee: 9740701, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 702, name: 'SATEC', codeInsee: 9740702, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 703, name: 'SIDR', codeInsee: 9740703, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 704, name: 'ZAC', codeInsee: 9740704, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 705, name: 'ZUP', codeInsee: 9740705, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 706, name: 'Rivière des Galets', codeInsee: 9740706, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 707, name: 'Zones Industrielles', codeInsee: 9740707, commune: { id: 7, name: 'Le Port' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      // La Possession
      { id: 801, name: 'Centre-ville', codeInsee: 9740801, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 802, name: 'ZAC Saint-Laurent', codeInsee: 9740802, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 803, name: 'Rivière des Galets', codeInsee: 9740803, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 804, name: 'Sainte-Thérèse', codeInsee: 9740804, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 805, name: 'Pichette', codeInsee: 9740805, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 806, name: 'Ravine à Malheur', codeInsee: 9740806, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 807, name: 'Dos d\'Ane', codeInsee: 9740807, commune: { id: 8, name: 'La Possession' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      // Saint-André
      { id: 901, name: 'Centre-ville', codeInsee: 9740901, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 902, name: 'Cambuston', codeInsee: 9740902, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 903, name: 'Champ Borne', codeInsee: 9740903, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 904, name: 'Rivière du Mât les Bas', codeInsee: 9740904, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 905, name: 'Ravine Creuse', codeInsee: 9740905, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 906, name: 'La Cressonière', codeInsee: 9740906, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 907, name: 'Mille Roches', codeInsee: 9740907, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 908, name: 'Bras des Chevrettes', codeInsee: 9740908, commune: { id: 9, name: 'Saint-André' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      // Saint-Benoît
      { id: 101, name: 'Centre-ville', codeInsee: 9741001, commune: { id: 10, name: 'Saint-Benoît' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 102, name: 'Sainte Anne', codeInsee: 9741002, commune: { id: 10, name: 'Saint-Benoît' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      // Saint-Denis
      { id: 1101, name: 'Centre-ville', codeInsee: 9741101, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1102, name: 'Bellepierre', codeInsee: 9741102, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1103, name: 'Le Brûlé', codeInsee: 9741103, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1104, name: 'Saint François', codeInsee: 9741104, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1105, name: 'Montgaillard', codeInsee: 9741105, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1106, name: 'Sainte-Clotilde', codeInsee: 9741106, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1107, name: 'Le Chaudron', codeInsee: 9741107, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1108, name: 'Moufia', codeInsee: 9741108, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1109, name: 'Bois de Nèfles', codeInsee: 9741109, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1110, name: 'La Bretagne', codeInsee: 9741110, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1111, name: 'Domenjod', codeInsee: 9741111, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1112, name: 'La Montagne', codeInsee: 9741112, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1113, name: 'Saint-Bernard', codeInsee: 9741113, commune: { id: 11, name: 'Saint-Denis' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      // Saint-Joseph
      { id: 1201, name: 'Centre-ville', codeInsee: 9741201, commune: { id: 12, name: 'Saint-Joseph' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 1202, name: 'Les Lianes', codeInsee: 9741202, commune: { id: 12, name: 'Saint-Joseph' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 1203, name: 'Plaine des Grègues', codeInsee: 9741203, commune: { id: 12, name: 'Saint-Joseph' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 1204, name: 'Jean Petit', codeInsee: 9741204, commune: { id: 12, name: 'Saint-Joseph' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 1205, name: 'Langevin', codeInsee: 9741205, commune: { id: 12, name: 'Saint-Joseph' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 1206, name: 'Vincendo', codeInsee: 9741206, commune: { id: 12, name: 'Saint-Joseph' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      // Saint-Leu
      { id: 1301, name: 'Centre-ville', codeInsee: 9741301, commune: { id: 13, name: 'Saint-Leu' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1302, name: 'Chaloupe', codeInsee: 9741302, commune: { id: 13, name: 'Saint-Leu' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1303, name: 'Le Piton', codeInsee: 9741303, commune: { id: 13, name: 'Saint-Leu' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1304, name: 'Le Plate', codeInsee: 9741304, commune: { id: 13, name: 'Saint-Leu' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      // Saint-Louis
      { id: 1401, name: 'Centre-ville', codeInsee: 9741401, commune: { id: 14, name: 'Saint-Louis' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1402, name: 'La Rivière', codeInsee: 9741402, commune: { id: 14, name: 'Saint-Louis' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1403, name: 'Le Gol', codeInsee: 9741403, commune: { id: 14, name: 'Saint-Louis' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1404, name: 'Ouaki', codeInsee: 9741404, commune: { id: 14, name: 'Saint-Louis' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1405, name: 'Les Makes', codeInsee: 9741405, commune: { id: 14, name: 'Saint-Louis' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      // Saint-Paul
      { id: 1501, name: 'Centre-ville', codeInsee: 9741501, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1502, name: 'Etang - Cambaie', codeInsee: 9741502, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1503, name: 'Grande Fontaine', codeInsee: 9741503, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1504, name: 'La Plaine', codeInsee: 9741504, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1505, name: 'Sans-Souci', codeInsee: 9741505, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1506, name: 'Bois de Nèfles', codeInsee: 9741506, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1507, name: 'Bellemène', codeInsee: 9741507, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1508, name: 'Bois Rouge', codeInsee: 9741508, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1509, name: 'Bernica', codeInsee: 9741509, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1510, name: 'Fleurimont - Plateau Caillou', codeInsee: 9741510, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1511, name: 'Saint-Gilles-Les-Hauts', codeInsee: 9741511, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1512, name: 'Saint-Gilles-Les-Bains', codeInsee: 9741512, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1513, name: 'La Saline-Les-Bains', codeInsee: 9741513, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1514, name: 'La Saline', codeInsee: 9741514, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1515, name: 'Barrage - Sacré Coeur', codeInsee: 9741515, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1516, name: 'Tan Rouge', codeInsee: 9741516, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 1517, name: 'Le Guillaume', codeInsee: 9741517, commune: { id: 15, name: 'Saint-Paul' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      // Saint-Pierre
      { id: 1601, name: 'Centre-ville', codeInsee: 9741601, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1602, name: 'Ravine Blanche - Pierrefonds', codeInsee: 9741602, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1603, name: 'Basse-Terre', codeInsee: 9741603, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1604, name: 'Ligne Paradis', codeInsee: 9741604, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1605, name: 'Bois d\'Olives', codeInsee: 9741605, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1606, name: 'Ravine des Cabris', codeInsee: 9741606, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1607, name: 'Ligne des Bambous', codeInsee: 9741607, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1608, name: 'Terre Sainte', codeInsee: 9741608, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1609, name: 'Grand Bois', codeInsee: 9741609, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1610, name: 'Montvert Les Bas', codeInsee: 9741610, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      { id: 1611, name: 'Montvert Les Hauts', codeInsee: 9741611, commune: { id: 16, name: 'Saint-Pierre' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() },
      // Saint-Philippe
      { id: 1701, name: 'Centre - Mare Longue', codeInsee: 9741701, commune: { id: 17, name: 'Saint-Philippe' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 1702, name: 'Basse Vallée', codeInsee: 9741702, commune: { id: 17, name: 'Saint-Philippe' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      // Sainte-Marie
      { id: 1801, name: 'Centre-ville', codeInsee: 9741801, commune: { id: 18, name: 'Sainte-Marie' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1802, name: 'Terrain Elisa - Beaumont', codeInsee: 9741802, commune: { id: 18, name: 'Sainte-Marie' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1803, name: 'La Ressource', codeInsee: 9741803, commune: { id: 18, name: 'Sainte-Marie' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1804, name: 'Grande Montée', codeInsee: 9741804, commune: { id: 18, name: 'Sainte-Marie' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1805, name: 'Rivière des Pluies', codeInsee: 9741805, commune: { id: 18, name: 'Sainte-Marie' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 1806, name: 'Gillot - La Mare', codeInsee: 9741806, commune: { id: 18, name: 'Sainte-Marie' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      // Sainte-Rose
      { id: 1901, name: 'Centre', codeInsee: 9741901, commune: { id: 19, name: 'Sainte-Rose' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 1902, name: 'Piton Sainte-Rose', codeInsee: 9741902, commune: { id: 19, name: 'Sainte-Rose' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 1903, name: 'Bois Blanc', codeInsee: 9741903, commune: { id: 19, name: 'Sainte-Rose' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 1904, name: 'Rivière de l\'Est', codeInsee: 9741904, commune: { id: 19, name: 'Sainte-Rose' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      // Sainte-Suzanne
      { id: 2001, name: 'Centre', codeInsee: 9742001, commune: { id: 20, name: 'Sainte-Suzanne' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 2002, name: 'Quartier Français', codeInsee: 9742002, commune: { id: 20, name: 'Sainte-Suzanne' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 2003, name: 'Deux-Rives', codeInsee: 9742003, commune: { id: 20, name: 'Sainte-Suzanne' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 2004, name: 'Bagatelle', codeInsee: 9742004, commune: { id: 20, name: 'Sainte-Suzanne' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 2005, name: 'Jacques Bel Air', codeInsee: 9742005, commune: { id: 20, name: 'Sainte-Suzanne' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      { id: 2006, name: 'Renaissance', codeInsee: 9742006, commune: { id: 20, name: 'Sainte-Suzanne' }, epci: { id: 2, name: 'CINOR' }, timestamp: new Date() },
      // Salazie
      { id: 2101, name: 'Centre', codeInsee: 9742101, commune: { id: 21, name: 'Salazie' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 2102, name: 'Grand Ilet', codeInsee: 9742102, commune: { id: 21, name: 'Salazie' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 2103, name: 'Hell Bourg', codeInsee: 9742103, commune: { id: 21, name: 'Salazie' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 2104, name: 'Mare à Vieille Place', codeInsee: 9742104, commune: { id: 21, name: 'Salazie' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      { id: 2105, name: 'Mare à Martin', codeInsee: 9742105, commune: { id: 21, name: 'Salazie' }, epci: { id: 3, name: 'CIREST' }, timestamp: new Date() },
      // Tampon
      { id: 2201, name: 'Centre-ville', codeInsee: 9742201, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2202, name: 'Trois Mares', codeInsee: 9742202, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2203, name: '14ème KM', codeInsee: 9742203, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2204, name: '12ème KM', codeInsee: 9742204, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2205, name: 'Terrain Fleury - La Pointe', codeInsee: 9742205, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2206, name: 'Bras de Pontho', codeInsee: 9742206, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2207, name: 'Pont d\'Yves', codeInsee: 9742207, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2208, name: '17ème KM', codeInsee: 9742208, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2209, name: 'Bras Creux', codeInsee: 9742209, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2210, name: 'Petit Tampon - Grand Tampon', codeInsee: 9742210, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2211, name: 'Bérive', codeInsee: 9742211, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      { id: 2212, name: 'Plaine des Cafres', codeInsee: 9742212, commune: { id: 22, name: 'Tampon' }, epci: { id: 1, name: 'CASUD' }, timestamp: new Date() },
      // Trois Bassins
      { id: 2301, name: 'Centre', codeInsee: 9742301, commune: { id: 23, name: 'Trois-Bassins' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      { id: 2302, name: 'Souris Blanche', codeInsee: 9742301, commune: { id: 23, name: 'Trois-Bassins' }, epci: { id: 5, name: 'TCO' }, timestamp: new Date() },
      // Cilaos
      { id: 2401, name: 'Cilaos', codeInsee: 9740201, commune: { id: 24, name: 'Cilaos' }, epci: { id: 4, name: 'CIVIS' }, timestamp: new Date() }
    ])

    // Je vérifie si tout s'est bien passé
    console.log(`Documents insérés => ${insertQuartiers} `)
  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données MongoDB', error)
  })
