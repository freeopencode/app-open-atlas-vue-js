import tcoModel from '../models/tco.js'

// Contrôleur pour envoyer le fichier GeoJSON
export const getTcoGeoJSON = async (req, res) => {
  const geoJSONData = await tcoModel.getAllGeoJSONData()
  if (geoJSONData) {
    res.setHeader('Content-Type', 'application/json')
    res.send(geoJSONData)
  } else {
    res.status(500).send('Internal Server Error')
  }
}
