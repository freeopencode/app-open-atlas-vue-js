import mongoose from 'mongoose'
import dotenv from 'dotenv'
import Country from '../model/Country.js'

// Connexion à la base de données

// La variable d'environnement MONGO_URL contient le lien pour se connecter à la database
dotenv.config({ path: '../../../backend/.env' })

// Récupérer l'URL de la base de données à partir des variables d'environnement
const mongoURL = process.env.MONGO_URL

// Connexion à la base de données MongoDB
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
// .then(async () => {
//   console.log('Connexion réussie à la base de données MongoDB')
//   // Interactions avec la base de données
//   const db = mongoose.connection
//   // Je crée une collection (équivalent de la table en SQL)

//   // J'insére les pays
//   const collectionCountries = db.collection('Countries')

//   // const insertCountries = await Country.insertOne(countryData)
//   const insertCountries = await collectionCountries.insertMany(countryData)

//   // Je vérifie si tout s'est bien passé
//   console.log(`Documents insérés => ${insertCountries} `)
// })
// .catch((error) => {
//   console.error('Erreur de connexion à la base de données MongoDB', error)
// })

// Créer des instances du modèle Country avec les données
const countryData = [
  { id: 1, name: 'Réunion', codeInsee: 974, timestamp: new Date() }
]

// Insérer les données dans la collection "Countries" à l'aide du modèle Country
Country.insertMany(countryData)
  .then((insertedCountries) => {
    // Je vérifie que toutes les données ont bien été reçues
    console.log(`Documents insérés => ${insertedCountries.length}`)
  })
  .catch((error) => {
    console.error('Erreur lors de l\'insetion des pays', error)
  })
