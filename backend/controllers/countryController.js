import Country from '../model/Country.js'

export const getCountries = async (req, res) => {
  try {
    // Récupérer tous les pays
    const countries = await Country.find()
    res.json(countries)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des pays' })
  }
}

export const updateCountry = async (req, res) => {
  const { id } = req.params
  const { name, codeInsee } = req.body

  try {
    const updatedCountry = await Country.findByIdAndUpdate(id, { name, codeInsee }, { new: true })

    if (!updatedCountry) {
      return res.status(404).json({ error: 'Pays non trouvé' })
    }

    res.json(updatedCountry)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour du pays' })
  }
}

export const deleteCountry = async (req, res) => {
  const { id } = req.params
  try {
    const deletedCountry = await Country.findByIdAndRemove(id)

    if (!deletedCountry) {
      return res.status(400).json({ error: 'Pays non trouvé' })
    }

    res.json({ message: 'Pays supprimé avec succès' })
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression du pays' })
  }
}
