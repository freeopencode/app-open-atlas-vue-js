import Commune from '../models/Commune.js'

export const getCommunes = async (req, res) => {
  try {
    // Récupérer toutes les communes
    const communes = await Commune.find()
    res.json(communes)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des communes' })
  }
}

export const updateCommune = async (req, res) => {
  const { id } = req.params
  const { name, codeInsee, epci } = req.body

  try {
    const updates = { name, codeInsee, epci }
    const updatedCommune = await Commune.updateCommune(id, updates)

    if (!updatedCommune) {
      return res.status(404).json({ error: 'Commune non trouvée' })
    }

    res.json(updatedCommune)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de la commune' })
  }
}

export const deleteCommune = async (req, res) => {
  const { id } = req.params
  try {
    const deletedCommune = await Commune.findByIdAndRemove(id)

    if (!deletedCommune) {
      return res.status(400).json({ error: 'Commune non trouvée' })
    }

    res.json({ message: 'Commune supprimée avec succès' })
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de la commune' })
  }
}
