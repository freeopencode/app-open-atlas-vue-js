import civisModel from '../models/civis.js'

// Contrôleur pour envoyer le fichier GeoJSON
export const getCivisGeoJSON = async (req, res) => {
  const geoJSONData = await civisModel.getAllGeoJSONData()
  if (geoJSONData) {
    res.setHeader('Content-Type', 'application/json')
    res.send(geoJSONData)
  } else {
    res.status(500).send('Internal Server Error')
  }
}
