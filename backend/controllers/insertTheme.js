import mongoose from 'mongoose'
import dotenv from 'dotenv'

// Connexion à la base de données

// La variable d'environnement MONGO_URL contient le lien pour se connecter à la database
dotenv.config({ path: '../../../backend/.env' })

// Récupérer l'URL de la base de données à partir des variables d'environnement
const mongoURL = process.env.MONGO_URL

// Connexion à la base de données MongoDB
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log('Connexion réussie à la base de données MongoDB')
    // Interactions avec la base de données
    const db = mongoose.connection
    // Je crée une collection (équivalent de la table en SQL)

    // J'insère les thèmes
    const collectionThemes = db.collection('Thèmes')
    const insertThemes = await collectionThemes.insertMany([
      { id: 1, name: 'Population', timestamp: new Date() },
      { id: 2, name: 'Economie', timestamp: new Date() },
      { id: 3, name: 'Tourisme', timestamp: new Date() },
      { id: 4, name: 'Social', timestamp: new Date() },
      { id: 5, name: 'Environnement', timestamp: new Date() },
      { id: 5, name: 'Sport et Loisirs', timestamp: new Date() },
      { id: 6, name: 'Santé', timestamp: new Date() },
      { id: 7, name: 'Sécurité', timestamp: new Date() },
      { id: 8, name: 'Cartographie', timestamp: new Date() }
    ])

    // Je vérifie si tout s'est bien passé
    console.log(`Documents insérés => ${insertThemes}`)
  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données MongoDB', error)
  })
