import Theme from '../model/Theme.js'

export const getThemes = async (req, res) => {
  try {
    // Récupérer tous les thèmes
    const themes = await Theme.find()
    res.json(themes)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération des thèmes' })
  }
}

export const updateTheme = async (req, res) => {
  const { id } = req.params
  const { name } = req.body

  try {
    const updatedTheme = await Theme.findByIdAndUpdate(id, { name }, { new: true })

    if (!updatedTheme) {
      return res.status(404).json({ error: 'Pays non trouvé' })
    }

    res.json(updatedTheme)
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour du thème' })
  }
}

export const deleteTheme = async (req, res) => {
  const { id } = req.params
  try {
    const deletedTheme = await Theme.findByIdAndRemove(id)

    if (!deletedTheme) {
      return res.status(400).json({ error: 'Thème non trouvé' })
    }

    res.json({ message: 'Thème supprimé avec succès' })
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression du thème' })
  }
}
