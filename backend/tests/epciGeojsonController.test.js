/* global describe, it */
// informe ESLINT que les variables sont globalement disponibles et ne doivent pas être signalées indéfinies

// epciGeojsonController.test.js

import { expect } from 'chai'
import request from 'supertest'
import app from '../index.js' // L'application Express serait exportée depuis un fichier index.js

describe('EpciGeojsonController', () => {
  it('should return HTML content for /epciGeojsonView', async () => {
    const response = await request(app).get('/epciGeojsonView')
    expect(response.status).to.equal(200)
    expect(response.type).to.equal('text/html')
  })

  it('should return JSON content for /epciGeojsonData', async () => {
    const response = await request(app).get('/epciGeojsonData')
    expect(response.status).to.equal(200)
    expect(response.type).to.equal('application/json')
  })

  it('should return HTML content width data for /epciGeoJSON', async () => {
    const response = await request(app).get('/epciGeoJSON')
    expect(response.status).to.equal(200)
    expect(response.type).to.equal('text/html')
    // Vérifiez que le contenu HTML contient les données attendues
    expect(response.text).to.include('Insert expected data here')
  })
})
