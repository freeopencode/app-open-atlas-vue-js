import dotenv from 'dotenv'

dotenv.config({ path: '../../../backend.env' })

const mongoURL = process.env.MONGO_URL

if (typeof process !== 'undefined') {
  // Code spécifique à l'environnement Node.js
  // On peut accéder à des variables d'environnement spécifiques à Node JS
  console.log('mongoURL', mongoURL)
} else {
  // Gérer le cas où process n'est pas disponible côté client
  console.log('Ce code ne doit pas être exécuté côté client')
}

export const config = {
  mongoURL
}
