# app-open-atlas-vue-js


## Nom du projet
Open Atlas avec Vue JS

## Description
Ceci est un projet de stage que j'ai réalisé du 23 septembre au 6 novembre 2023 au sein de l'association Open Atlas dans le cadre de l'obtention du Titre Professionnel Développeur Web & Mobile. Par la suite, j'ai amélioré ce projet initial en corrigeant les bugs et en y ajoutant des fonctionnalités. 

Ce projet concerne l'univers de la Dataviz. Il s'agit de représenter des données issues d'APIs open source sous forme de tableaux, de graphiques et de cartes. 

## Badges

En cours de développement

## Visuals

Bientôt disponible

## Installation

### Prérequis

Assurez-vous d'avoir Node.js et npm installés sur votre machine.

### Démarrage

1. Clonez le dépôt
```
git clone https://gitlab.com/freeopencode/app-open-atlas-vue-js.git
```

2. Installer les dépendances
```
npm install
```

### Backend

Pour démarrer le serveur backend, utilisez la commande suivante :

En développement
```
npm start
```

En production
```
npm run build
```

### Frontend

Pour démarrer l'application frontend, utilisez la commande suivante :

En développement
```
npm run serve
```

En production
```
npm run build
```

## Usage

L'application permet de visualiser des données sous forme de tableaux, de graphiques et de cartes. Vous pouvez interagir avec les différents éléments pour explorer les données en détail.

## Support

Pour toute demande d'aide ou de support, veuillez contacter l'auteur du projet.

## Roadmap

Des fonctionnalités supplémentaires et des améliorations sont prévues pour les futures versions.

## Contributing

Les contributions sont les bienvenues. Pour proposer des modifications, veillez ouvrir une pull request.

## Authors and acknowledgment

Auteur : Sylvie HOAREAU

## License

Ce projet est open source sous licence GNU GPL 3.0.

## Project status

Le projet est en cours de développement.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/freeopencode/app-open-atlas-vue-js.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/freeopencode/app-open-atlas-vue-js/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
