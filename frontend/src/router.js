// import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'

import HomePage from './components/pages/HomePage'
import PageSourcesApis from './components/pages/PageSourcesApis'
import ReunionOSM from './components/maps/ReunionOSM'
import ReunionCarte from './components/themes/reunion/ReunionCarte'
import MapReunionIGNv2 from './components/maps/MapReunionIGNv2'
import MapReunionIGNAdmin from './components/maps/ign/MapReunionIGNAdmin'
import MapReunionIGNIris from './components/maps/ign/MapReunionIGNIris'
import MapReunionIGNandSDK from './components/maps/ign/MapReunionIGNandSDK'
import MapReunionZnieff from './components/maps/reunion/region/MapReunionZnieff'
// Pages principales
import PageNotFound from './components/pages/PageNotFound'

// Pages des thèmes
import paysRoutes from './routes/reunion/reunionRouter.js'
import epcisRoutes from './routes/reunion/epciRouter.js'
import communesRoutes from './routes/reunion/communeRouter.js'

const routes = [
  // Routes spécifiques avec paramètres
  ...paysRoutes,
  ...epcisRoutes,
  ...communesRoutes,

  // Routes génériques
  // {
  //   path: '/',
  //   name: 'app',
  //   component: App
  // },
  {
    path: '/',
    name: 'home',
    component: HomePage, // Composant de la page d'accueil
    props: true, // Pour transmettre automatiquement les paramètres
    meta: { breadcrumb: 'Accueil' } // Etiquette du breadcrumb
  },
  {
    path: '/sources',
    name: 'sources',
    component: PageSourcesApis,
    props: true
  },
  {
    path: '/pays/:paysName/carte',
    name: 'carteReunion',
    component: ReunionCarte,
    props: (route) => ({
      pays: route.params.pays
    })
  },
  {
    path: '/pays/:paysName/carte/OSM',
    name: 'carteOSM',
    component: ReunionOSM,
    props: true
  },
  {
    path: '/pays/:paysName/carte/IGN',
    name: 'carteIGN',
    component: MapReunionIGNv2,
    props: true
  },
  {
    path: '/pays/:paysName/carte/IGN',
    name: 'carteIGN',
    component: MapReunionIGNandSDK,
    props: true
  },
  {
    path: '/pays/:paysName/carte/IGN',
    name: 'carteAdmin',
    component: MapReunionIGNAdmin,
    props: true
  },
  {
    path: '/pays/:paysName/carte/Iris',
    name: 'carteIris',
    component: MapReunionIGNIris,
    props: true
  },
  {
    path: '/pays/:paysName/carte/Znieff',
    name: 'carteZnieff',
    component: MapReunionZnieff,
    props: true
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: PageNotFound
  }

]
const router = createRouter({
  history: createWebHistory(),
  routes
})

const originalPush = router.push

router.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
