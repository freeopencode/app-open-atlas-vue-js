// module Vuex commune.js

const communeModule = {
  state: () => ({
    showThemeLinks: {},
    themes: {
      Amenagement: { name: 'Amenagement', value: 'amenagement' },
      Carte: { name: 'Carte', value: 'carte' },
      Economie: { name: 'Economie', value: 'economie' },
      Environnement: { name: 'Environnement', value: 'environnement' },
      Population: { name: 'Population', value: 'population' },
      Social: { name: 'Social', value: 'social' },
      Sport: { name: 'Sport', value: 'sport' },
      Tourisme: { name: 'Tourisme', value: 'tourisme' }
    },
    subjects: {
      communeBati: {
        id: 1,
        name: 'communeBati',
        label: 'Aspect du bâti',
        value: 'communeBati',
        theme: 'amenagement'
      },
      communeStationnement: {
        id: 2,
        name: 'communeStationnement',
        label: 'Emplacements réservés au stationnement',
        value: 'communeStationnement',
        theme: 'amenagement'
      },
      communeFloresNA17: {
        id: 3,
        name: 'communeFloresNA17',
        label: 'Activités économiques en 17 postes',
        value: 'communeFloresNA17',
        theme: 'economie'
      },
      commune5G: {
        id: 4,
        name: 'commune5G',
        label: 'Antennes 5G',
        value: 'commune5G',
        theme: 'economie'
      },
      communeSirene: {
        id: 5,
        name: 'communeSirene',
        label: 'Liste des entreprises (SIRENE)',
        value: 'communeSirene',
        theme: 'economie'
      },
      communeVoiture: {
        id: 6,
        name: 'communeVoiture',
        label: 'Equipements des ménages en voiture',
        value: 'communeVoiture',
        theme: 'economie'
      },
      communeRisques: {
        id: 7,
        name: 'communeRisques',
        label: 'Opérations sur les risques',
        value: 'communeRisques',
        theme: 'environnement'
      },
      communePopLeg: {
        id: 8,
        name: 'communePopLeg',
        label: 'Populations Légales',
        value: 'communePopLeg',
        theme: 'population'
      },
      communeEtabSante: {
        id: 9,
        name: 'communeEtabSante',
        label: 'Etablissements de santé',
        value: 'communeEtabSante',
        theme: 'sante'
      },
      communeFamille: {
        id: 10,
        name: 'communeFamille',
        label: 'Types de famille',
        value: 'communeFamille',
        theme: 'social'
      },
      communeBassinNat: {
        id: 11,
        name: 'communeBassinNat',
        label: 'Les bassins de natation',
        value: 'communeBassinNat',
        theme: 'sport'
      },
      communeEtabTourisme: {
        id: 12,
        name: 'communeEtabTourisme',
        label: 'Etablissements touristiques',
        value: 'communeEtabTourisme',
        theme: 'tourisme'
      },
      communeHebTourisme: {
        id: 13,
        name: 'communeHebTourisme',
        label: 'Hébergement touristiques',
        value: 'communeHebTourisme',
        theme: 'tourisme'
      }
    }
    // selectedTheme: 'amenagement',
    // selectedSubject: 'communeBati'
  }),
  mutations: {
    // Mutations
    // toggleEpciLink (state, epciName) {
    //   // Inverser la valeur de showEpciLinks pour l'EPCI spécifié
    //   state.showEpciLinks[epciName] = !state.showEpciLinks[epciName]
    // },
    // Nouvelle mutation pour mettre à jour le sujet sélectionné
    setSelectedSubject (state, subject) {
      state.selectedSubject = subject
    },
    updateShowThemeLinks (state, themeLinks) {
      state.showThemeLinks = themeLinks
    },
    updateSelectedTheme (state, theme) {
      state.selectedTheme = theme
      state.selectedSubject = null // Réinitialise le sujet sélectionné
    },
    updateSelectedSubject (state, subject) {
      state.selectedSubject = subject
    },
    // Nouveau code
    updateThemes (state, themes) {
      // Utiliser un Set pour stocker les thèmes uniques
      const uniqueThemes = new Set(Object.keys(themes))

      // Mettre à jour les thèmes dans le state
      state.themes = Object.fromEntries(Array.from(uniqueThemes).map(theme => [theme, { name: theme, value: themes[theme].value }]))
    },
    updateSubjects (state, subjects) {
      // Utiliser un Set pour stocker les sujets uniques
      const uniqueSubjects = new Set(subjects.map(subject => subject.value))

      // Mettre à jour les sujets dans le state
      state.subjects = Object.fromEntries(Array.from(uniqueSubjects).map(subjectValue => {
        const subject = subjects.find(subject => subject.value === subjectValue)
        return [subjectValue, { ...subject }]
      }))
    }
  },
  actions: {
    // Actions
    // Requête API pour obtenir les liens des thèmes
    async fetchThemeLinks (context) {
      try {
        const themeLinks = await new Promise(resolve => {
          setTimeout(() => {
            resolve({
              Amenagement: true,
              Carte: false,
              Economie: false,
              Environnement: false,
              Population: false,
              Social: false,
              Sport: false,
              Tourisme: false
            })
          }, 3000)
        })
        context.commit('updateShowThemeLinks', themeLinks)
      } catch (error) {
        console.error('Erreur fetching theme links:', error)
      }
    },
    // Obtenir la liste des sujets
    async fetchSubjects (context) {
      try {
        // Requête API pour obtenir la liste des sujets
        const subjects = await new Promise(resolve => {
          setTimeout(() => {
            resolve([
              {
                id: 1,
                name: 'communeBati',
                label: 'Aspect du bâti',
                value: 'communeBati',
                theme: 'amenagement'
              },
              {
                id: 2,
                name: 'communeStationnement',
                label: 'Emplacements réservés au stationnement',
                value: 'communeStationnement',
                theme: 'amenagement'
              },
              {
                id: 3,
                name: 'communeFloresNA17',
                label: 'Activités économiques en 17 postes',
                value: 'communeFloresNA17',
                theme: 'economie'
              },
              {
                id: 4,
                name: 'commune5G',
                label: 'Antennes 5G',
                value: 'commune5G',
                theme: 'economie'
              },
              {
                id: 5,
                name: 'communeSirene',
                label: 'Liste des entreprises (SIRENE)',
                value: 'communeSirene',
                theme: 'economie'
              },
              {
                id: 6,
                name: 'communeVoiture',
                label: 'Equipements des ménages en voiture',
                value: 'communeVoiture',
                theme: 'economie'
              },
              {
                id: 7,
                name: 'communeRisques',
                label: 'Opérations sur les risques',
                value: 'communeRisques',
                theme: 'environnement'
              },
              {
                id: 8,
                name: 'communePopLeg',
                label: 'Populations Légales',
                value: 'communePopLeg',
                theme: 'population'
              },
              {
                id: 9,
                name: 'communeEtabSante',
                label: 'Etablissements de santé',
                value: 'communeEtabSante',
                theme: 'sante'
              },
              {
                id: 10,
                name: 'communeFamille',
                label: 'Types de famille',
                value: 'communeFamille',
                theme: 'social'
              },
              {
                id: 11,
                name: 'communeBassinNat',
                label: 'Les bassins de natation',
                value: 'communeBassinNat',
                theme: 'sport'
              },
              {
                id: 12,
                name: 'communeEtabTourisme',
                label: 'Etablissements touristiques',
                value: 'communeEtabTourisme',
                theme: 'tourisme'
              },
              {
                id: 13,
                name: 'communeHebTourisme',
                label: 'Hébergement touristiques',
                value: 'communeHebTourisme',
                theme: 'tourisme'
              }
            ])
          })
        })
        context.commit('updateSubjects', subjects)
      } catch (error) {
        console.error('Error fetching subjects:', error)
      }
    },
    // Pour mettre à jour le sujet sélectionné
    updateSelectedSubjectAction (context, value) {
      // Déclencher la mutation pour mettre à jour le sujet sélectionné
      context.commit('updateSelectedSubject', value)
    }
  },
  getters: {
    // Getters
    // showEpciLinks (state) {
    //   return state.showEpciLinks
    // },
    showThemeLinks (state) {
      return state.showThemeLinks
    },
    // Getter pour récupérer le sujet sélectionné
    selectedSubject (state) {
      return state.selectedSubject
    },
    subjects (state) {
      return state.subjects
    }
  }
}

export default communeModule
