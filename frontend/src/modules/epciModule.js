// module Vuex epciModule.js
const epciModule = {
  state: () => ({
    showThemeLinks: {},
    themes: {
      Amenagement: { name: 'Amenagement', value: 'amenagement' },
      Carte: { name: 'Carte', value: 'carte' },
      Economie: { name: 'Economie', value: 'economie' },
      Environnement: { name: 'Environnement', value: 'environnement' },
      Population: { name: 'Population', value: 'population' },
      Social: { name: 'Social', value: 'social' },
      Sport: { name: 'Sport', value: 'sport' },
      Tourisme: { name: 'Tourisme', value: 'tourisme' }
    },
    subjects: {
      epciBati: {
        id: 1,
        name: 'epciBati',
        label: 'Aspect du bâti',
        value: 'epciBati',
        theme: 'amenagement'
      },
      epciStationnement: {
        id: 2,
        name: 'epciStationnement',
        label: 'Emplacements réservés au stationnement',
        value: 'epciStationnement',
        theme: 'amenagement'
      },
      epciFloresNA17: {
        id: 3,
        name: 'epciFloresNA17',
        label: 'Activités économiques en 17 postes',
        value: 'epciFloresNA17',
        theme: 'economie'
      },
      epci5G: {
        id: 4,
        name: 'epci5G',
        label: 'Antennes 5G',
        value: 'epci5G',
        theme: 'economie'
      },
      epciSirene: {
        id: 5,
        name: 'epciSirene',
        label: 'Liste des entreprises (SIRENE)',
        value: 'epciSirene',
        theme: 'economie'
      },
      epciVoiture: {
        id: 6,
        name: 'epciVoiture',
        label: 'Equipements des ménages en voiture',
        value: 'epciVoiture',
        theme: 'economie'
      },
      epciRisques: {
        id: 7,
        name: 'epciRisques',
        label: 'Opérations sur les risques',
        value: 'epciRisques',
        theme: 'environnement'
      },
      epciPopLeg: {
        id: 8,
        name: 'epciPopLeg',
        label: 'Populations Légales',
        value: 'epciPopLeg',
        theme: 'population'
      },
      epciEtabSante: {
        id: 9,
        name: 'epciEtabSante',
        label: 'Etablissements de santé',
        value: 'epciEtabSante',
        theme: 'sante'
      },
      epciFamille: {
        id: 10,
        name: 'epciFamille',
        label: 'Types de famille',
        value: 'epciFamille',
        theme: 'social'
      },
      epciBassinNat: {
        id: 11,
        name: 'epciBassinNat',
        label: 'Les bassins de natation',
        value: 'epciBassinNat',
        theme: 'sport'
      },
      epciEtabTourisme: {
        id: 12,
        name: 'epciEtabTourisme',
        label: 'Etablissements touristiques',
        value: 'epciEtabTourisme',
        theme: 'tourisme'
      },
      epciHebTourisme: {
        id: 13,
        name: 'epciHebTourisme',
        label: 'Hébergement touristiques',
        value: 'epciHebTourisme',
        theme: 'tourisme'
      }
    }
    // selectedTheme: 'amenagement',
    // selectedSubject: 'epciBati'
  }),
  mutations: {
    // Mutations
    // toggleEpciLink (state, epciName) {
    //   // Inverser la valeur de showEpciLinks pour l'EPCI spécifié
    //   state.showEpciLinks[epciName] = !state.showEpciLinks[epciName]
    // },
    // Nouvelle mutation pour mettre à jour le sujet sélectionné
    setSelectedSubject (state, subject) {
      state.selectedSubject = subject
    },
    updateShowThemeLinks (state, themeLinks) {
      state.showThemeLinks = themeLinks
    },
    updateSelectedTheme (state, theme) {
      state.selectedTheme = theme
      state.selectedSubject = null // Réinitialise le sujet sélectionné
    },
    updateSelectedSubject (state, subject) {
      state.selectedSubject = subject
    },
    // Nouveau code
    updateThemes (state, themes) {
      // Utiliser un Set pour stocker les thèmes uniques
      const uniqueThemes = new Set(Object.keys(themes))

      // Mettre à jour les thèmes dans le state
      state.themes = Object.fromEntries(Array.from(uniqueThemes).map(theme => [theme, { name: theme, value: themes[theme].value }]))
    },
    updateSubjects (state, subjects) {
      // Utiliser un Set pour stocker les sujets uniques
      const uniqueSubjects = new Set(subjects.map(subject => subject.value))

      // Mettre à jour les sujets dans le state
      state.subjects = Object.fromEntries(Array.from(uniqueSubjects).map(subjectValue => {
        const subject = subjects.find(subject => subject.value === subjectValue)
        return [subjectValue, { ...subject }]
      }))
    }
  },
  actions: {
    // Actions
    // Requête API pour obtenir les liens des thèmes
    async fetchThemeLinks (context) {
      try {
        const themeLinks = await new Promise(resolve => {
          setTimeout(() => {
            resolve({
              Amenagement: true,
              Carte: false,
              Economie: false,
              Environnement: false,
              Population: false,
              Social: false,
              Sport: false,
              Tourisme: false
            })
          }, 3000)
        })
        context.commit('updateShowThemeLinks', themeLinks)
      } catch (error) {
        console.error('Erreur fetching theme links:', error)
      }
    },
    // Obtenir la liste des sujets
    async fetchSubjects (context) {
      try {
        // Requête API pour obtenir la liste des sujets
        const subjects = await new Promise(resolve => {
          setTimeout(() => {
            resolve([
              {
                id: 1,
                name: 'epciBati',
                label: 'Aspect du bâti',
                value: 'epciBati',
                theme: 'amenagement'
              },
              {
                id: 2,
                name: 'epciStationnement',
                label: 'Emplacements réservés au stationnement',
                value: 'epciStationnement',
                theme: 'amenagement'
              },
              {
                id: 3,
                name: 'epciFloresNA17',
                label: 'Activités économiques en 17 postes',
                value: 'epciFloresNA17',
                theme: 'economie'
              },
              {
                id: 4,
                name: 'epci5G',
                label: 'Antennes 5G',
                value: 'epci5G',
                theme: 'economie'
              },
              {
                id: 5,
                name: 'epciSirene',
                label: 'Liste des entreprises (SIRENE)',
                value: 'epciSirene',
                theme: 'economie'
              },
              {
                id: 6,
                name: 'epciVoiture',
                label: 'Equipements des ménages en voiture',
                value: 'epciVoiture',
                theme: 'economie'
              },
              {
                id: 7,
                name: 'epciRisques',
                label: 'Opérations sur les risques',
                value: 'epciRisques',
                theme: 'environnement'
              },
              {
                id: 8,
                name: 'epciPopLeg',
                label: 'Populations Légales',
                value: 'epciPopLeg',
                theme: 'population'
              },
              {
                id: 9,
                name: 'epciEtabSante',
                label: 'Etablissements de santé',
                value: 'epciEtabSante',
                theme: 'sante'
              },
              {
                id: 10,
                name: 'epciFamille',
                label: 'Types de famille',
                value: 'epciFamille',
                theme: 'social'
              },
              {
                id: 11,
                name: 'epciBassinNat',
                label: 'Les bassins de natation',
                value: 'epciBassinNat',
                theme: 'sport'
              },
              {
                id: 12,
                name: 'epciEtabTourisme',
                label: 'Etablissements touristiques',
                value: 'epciEtabTourisme',
                theme: 'tourisme'
              },
              {
                id: 13,
                name: 'epciHebTourisme',
                label: 'Hébergement touristiques',
                value: 'epciHebTourisme',
                theme: 'tourisme'
              }
            ])
          })
        })
        context.commit('updateSubjects', subjects)
      } catch (error) {
        console.error('Error fetching subjects:', error)
      }
    },
    // Pour mettre à jour le sujet sélectionné
    updateSelectedSubjectAction (context, value) {
      // Déclencher la mutation pour mettre à jour le sujet sélectionné
      context.commit('updateSelectedSubject', value)
    }
  },
  getters: {
    // Getters
    // showEpciLinks (state) {
    //   return state.showEpciLinks
    // },
    showThemeLinks (state) {
      return state.showThemeLinks
    },
    // Getter pour récupérer le sujet sélectionné
    selectedSubject (state) {
      return state.selectedSubject
    },
    subjects (state) {
      return state.subjects
    }
  }
}

export default epciModule
