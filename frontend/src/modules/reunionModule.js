// module Vuex reunionModule.js

const reunionModule = {
  state: () => ({
    showThemeLinks: {},
    themes: {
      Amenagement: { name: 'Amenagement', value: 'amenagement' },
      Carte: { name: 'Carte', value: 'carte' },
      Economie: { name: 'Economie', value: 'economie' },
      Environnement: { name: 'Environnement', value: 'environnement' },
      Population: { name: 'Population', value: 'population' },
      Social: { name: 'Social', value: 'social' },
      Sport: { name: 'Sport', value: 'sport' },
      Tourisme: { name: 'Tourisme', value: 'tourisme' }
    },
    subjects: {
      communeBati: {
        id: 1,
        name: 'reunionBati',
        label: 'Aspect du bâti',
        value: 'reunionBati',
        theme: 'amenagement'
      },
      communeStationnement: {
        id: 2,
        name: 'reunionStationnement',
        label: 'Emplacements réservés au stationnement',
        value: 'reunionStationnement',
        theme: 'amenagement'
      },
      communeFloresNA17: {
        id: 3,
        name: 'reunionFloresNA17',
        label: 'Activités économiques en 17 postes',
        value: 'reunionFloresNA17',
        theme: 'economie'
      },
      commune5G: {
        id: 4,
        name: 'reunion5G',
        label: 'Antennes 5G',
        value: 'reunion5G',
        theme: 'economie'
      },
      communeSirene: {
        id: 5,
        name: 'reunionSirene',
        label: 'Liste des entreprises (SIRENE)',
        value: 'reunionSirene',
        theme: 'economie'
      },
      communeVoiture: {
        id: 6,
        name: 'reunionVoiture',
        label: 'Equipements des ménages en voiture',
        value: 'reunionVoiture',
        theme: 'economie'
      },
      communeRisques: {
        id: 7,
        name: 'reunionRisques',
        label: 'Opérations sur les risques',
        value: 'reunionRisques',
        theme: 'environnement'
      },
      communePopLeg: {
        id: 8,
        name: 'reunionPopLeg',
        label: 'Populations Légales',
        value: 'reunionPopLeg',
        theme: 'population'
      },
      communeEtabSante: {
        id: 9,
        name: 'reunionEtabSante',
        label: 'Etablissements de santé',
        value: 'reunionEtabSante',
        theme: 'sante'
      },
      communeFamille: {
        id: 10,
        name: 'reunionFamille',
        label: 'Types de famille',
        value: 'reunionFamille',
        theme: 'social'
      },
      communeBassinNat: {
        id: 11,
        name: 'reunionBassinNat',
        label: 'Les bassins de natation',
        value: 'reunionBassinNat',
        theme: 'sport'
      },
      communeEtabTourisme: {
        id: 12,
        name: 'reunionEtabTourisme',
        label: 'Etablissements touristiques',
        value: 'reunionEtabTourisme',
        theme: 'tourisme'
      },
      communeHebTourisme: {
        id: 13,
        name: 'reunionHebTourisme',
        label: 'Hébergements touristiques',
        value: 'reunionHebTourisme',
        theme: 'tourisme'
      }
    }
    // selectedTheme: 'amenagement',
    // selectedSubject: 'reunionBati'
  }),
  mutations: {
    // Mutations
    // toggleEpciLink (state, epciName) {
    //   // Inverser la valeur de showEpciLinks pour l'EPCI spécifié
    //   state.showEpciLinks[epciName] = !state.showEpciLinks[epciName]
    // },
    // Nouvelle mutation pour mettre à jour le sujet sélectionné
    setSelectedSubject (state, subject) {
      state.selectedSubject = subject
    },
    updateShowThemeLinks (state, themeLinks) {
      state.showThemeLinks = themeLinks
    },
    updateSelectedTheme (state, theme) {
      state.selectedTheme = theme
      state.selectedSubject = null // Réinitialise le sujet sélectionné
    },
    updateSelectedSubject (state, subject) {
      state.selectedSubject = subject
    },
    // Nouveau code
    updateThemes (state, themes) {
      // Utiliser un Set pour stocker les thèmes uniques
      const uniqueThemes = new Set(Object.keys(themes))

      // Mettre à jour les thèmes dans le state
      state.themes = Object.fromEntries(Array.from(uniqueThemes).map(theme => [theme, { name: theme, value: themes[theme].value }]))
    },
    updateSubjects (state, subjects) {
      // Utiliser un Set pour stocker les sujets uniques
      const uniqueSubjects = new Set(subjects.map(subject => subject.value))

      // Mettre à jour les sujets dans le state
      state.subjects = Object.fromEntries(Array.from(uniqueSubjects).map(subjectValue => {
        const subject = subjects.find(subject => subject.value === subjectValue)
        return [subjectValue, { ...subject }]
      }))
    }
  },
  actions: {
    // Actions
    // Requête API pour obtenir les liens des thèmes
    async fetchThemeLinks (context) {
      try {
        const themeLinks = await new Promise(resolve => {
          setTimeout(() => {
            resolve({
              Amenagement: true,
              Carte: false,
              Economie: false,
              Environnement: false,
              Population: false,
              Social: false,
              Sport: false,
              Tourisme: false
            })
          }, 3000)
        })
        context.commit('updateShowThemeLinks', themeLinks)
      } catch (error) {
        console.error('Erreur fetching theme links:', error)
      }
    },
    // Obtenir la liste des sujets
    async fetchSubjects (context) {
      try {
        // Requête API pour obtenir la liste des sujets
        const subjects = await new Promise(resolve => {
          setTimeout(() => {
            resolve([
              {
                id: 1,
                name: 'reunionBati',
                label: 'Aspect du bâti',
                value: 'reunionBati',
                theme: 'amenagement'
              },
              {
                id: 2,
                name: 'reunionStationnement',
                label: 'Emplacements réservés au stationnement',
                value: 'reunionStationnement',
                theme: 'amenagement'
              },
              {
                id: 3,
                name: 'reunionFloresNA17',
                label: 'Activités économiques en 17 postes',
                value: 'reunionFloresNA17',
                theme: 'economie'
              },
              {
                id: 4,
                name: 'reunion5G',
                label: 'Antennes 5G',
                value: 'reunion5G',
                theme: 'economie'
              },
              {
                id: 5,
                name: 'reunionSirene',
                label: 'Liste des entreprises (SIRENE)',
                value: 'reunionSirene',
                theme: 'economie'
              },
              {
                id: 6,
                name: 'reunionVoiture',
                label: 'Equipements des ménages en voiture',
                value: 'reunionVoiture',
                theme: 'economie'
              },
              {
                id: 7,
                name: 'reunionRisques',
                label: 'Opérations sur les risques',
                value: 'reunionRisques',
                theme: 'environnement'
              },
              {
                id: 8,
                name: 'reunionPopLeg',
                label: 'Populations Légales',
                value: 'reunionPopLeg',
                theme: 'population'
              },
              {
                id: 9,
                name: 'reunionEtabSante',
                label: 'Etablissements de santé',
                value: 'reunionEtabSante',
                theme: 'sante'
              },
              {
                id: 10,
                name: 'reunionFamille',
                label: 'Types de famille',
                value: 'reunionFamille',
                theme: 'social'
              },
              {
                id: 11,
                name: 'reunionBassinNat',
                label: 'Les bassins de natation',
                value: 'reunionBassinNat',
                theme: 'sport'
              },
              {
                id: 12,
                name: 'reunionEtabTourisme',
                label: 'Etablissements touristiques',
                value: 'reunionEtabTourisme',
                theme: 'tourisme'
              },
              {
                id: 13,
                name: 'reunionHebTourisme',
                label: 'Hébergement touristiques',
                value: 'reunionHebTourisme',
                theme: 'tourisme'
              }
            ])
          })
        })
        context.commit('updateSubjects', subjects)
      } catch (error) {
        console.error('Error fetching subjects:', error)
      }
    },
    // Pour mettre à jour le sujet sélectionné
    updateSelectedSubjectAction (context, value) {
      // Déclencher la mutation pour mettre à jour le sujet sélectionné
      context.commit('updateSelectedSubject', value)
    }
  },
  getters: {
    // Getters
    // showEpciLinks (state) {
    //   return state.showEpciLinks
    // },
    showThemeLinks (state) {
      return state.showThemeLinks
    },
    // Getter pour récupérer le sujet sélectionné
    selectedSubject (state) {
      return state.selectedSubject
    },
    subjects (state) {
      return state.subjects
    }
  }
}

export default reunionModule
