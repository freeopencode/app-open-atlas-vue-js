// CommuneSubjectStore.js

import { defineStore } from 'pinia'

export const useCommuneSubjectStore = defineStore(
  'communeSubject', {
    state: () => ({
      subjects: [
        {
          id: 1,
          name: 'amenagement',
          label: 'Aménagement',
          subjects: [
            { id: 1, name: 'communeBati', label: 'L\'aspect du bâti' },
            { id: 2, name: 'communeStationnement', label: 'Les emplacements de stationnement' }
          ]
        },
        {
          id: 2,
          name: 'carte',
          label: 'Carte',
          subjects: [
            { id: 1, name: 'communeClicMap', label: 'Carte clicable' },
            { id: 2, name: 'communeIGNandSDK', label: 'Orthophoto de l\'IGN' }
          ]
        },
        {
          id: 3,
          name: 'economie',
          label: 'Economie',
          subjects: [
            { id: 1, name: 'commune5G', label: 'Sites mobiles 5G' },
            { id: 2, name: 'communeVoiture', label: 'Equipements des ménages en voitures' },
            { id: 3, name: 'communeFloresNA17', label: 'Activités économiques en 17 postes' },
            { id: 4, name: 'communeSirene', label: 'Liste des entreprises (SIRENE)' }
          ]
        },
        {
          id: 4,
          name: 'environnement',
          label: 'Environnement',
          subjects: [
            { id: 1, name: 'communeRisques', label: 'Les risques' },
            { id: 2, name: 'communeCatNat', label: 'Les catastrophes naturelles' }
          ]
        },
        {
          id: 5,
          name: 'population',
          label: 'Population',
          subjects: [
            { id: 1, name: 'communePopLeg', label: 'Les populations légales' },
            { id: 2, name: 'communeFamille', label: 'Les types de familles' }
          ]
        },
        {
          id: 6,
          name: 'sport',
          label: 'Sport',
          subjects: [
            { id: 1, name: 'communeBassinNat', label: 'Les bassins de natation' },
            { id: 2, name: 'communeEquipementSport', label: 'Les équipements sportifs à La Réunion' }
          ]
        },
        {
          id: 7,
          name: 'tourisme',
          label: 'Tourisme',
          subjects: [
            { id: 1, name: 'communeEtabTourisme', label: 'Les établissements touristiques' },
            { id: 2, name: 'communeHebTourisme', label: 'Les hebergements touristiques' }
          ]
        }
      ],
      selectedSubject: null
    }),
    actions: {
      setSubjects (subjects) {
        this.subjects = subjects
      },
      setSelectedSubject (subject) {
        this.selectedSubject = subject
      }
    }
  })
