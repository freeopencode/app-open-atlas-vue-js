// stores/scaleStore.js

import { defineStore } from 'pinia'

export const useScaleStore = defineStore('scaleStore', {
  state: () => ({
    selectedEpci: null
  }),
  actions: {
    updateSelectedEpci (epci) {
      this.selectedEpci = epci
    }
  }
})
