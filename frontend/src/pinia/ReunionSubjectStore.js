// ReunionSubjectStore.js

import { defineStore } from 'pinia'

export const useReunionSubjectStore = defineStore(
  'reunionSubject', {
    state: () => ({
      subjects: [
        {
          id: 1,
          name: 'amenagement',
          label: 'Aménagement',
          subjects: [
            { id: 1, name: 'reunionBati', label: 'L\'aspect du bâti' },
            { id: 2, name: 'reunionStationnement', label: 'Les emplacements de stationnement' }
          ]
        },
        {
          id: 2,
          name: 'carte',
          label: 'Carte',
          subjects: [
            { id: 1, name: 'reunionClicMap', label: 'Carte clicable' },
            { id: 2, name: 'reunionIGNandSDK', label: 'Orthophoto de l\'IGN' }
          ]
        },
        {
          id: 3,
          name: 'economie',
          label: 'Economie',
          subjects: [
            { id: 1, name: 'reunion5G', label: 'Sites mobiles 5G' },
            { id: 2, name: 'reunionVoiture', label: 'Equipements des ménages en voitures' },
            { id: 3, name: 'reunionFloresNA17', label: 'Activités économiques en 17 postes' },
            { id: 4, name: 'reunionSirene', label: 'Liste des entreprises (SIRENE)' }
          ]
        },
        {
          id: 4,
          name: 'environnement',
          label: 'Environnement',
          subjects: [
            { id: 1, name: 'reunionRisques', label: 'Les risques' },
            { id: 2, name: 'reunionCatNat', label: 'Les catastrophes naturelles' }
          ]
        },
        {
          id: 5,
          name: 'population',
          label: 'Population',
          subjects: [
            { id: 1, name: 'reunionPopLeg', label: 'Les populations légales' },
            { id: 2, name: 'reunionFamille', label: 'Les types de familles' }
          ]
        },
        {
          id: 6,
          name: 'sport',
          label: 'Sport',
          subjects: [
            { id: 1, name: 'reunionBassinNat', label: 'Les bassins de natation' },
            { id: 2, name: 'reunionEquipementSport', label: 'Les équipements sportifs à La Réunion' }
          ]
        },
        {
          id: 7,
          name: 'tourisme',
          label: 'Tourisme',
          subjects: [
            { id: 1, name: 'reunionEtabTourisme', label: 'Les établissements touristiques' },
            { id: 2, name: 'reunionHebTourisme', label: 'Les hebergements touristiques' }
          ]
        }
      ],
      selectedSubject: null
    }),
    actions: {
      setSubjects (subjects) {
        this.subjects = subjects
      },
      setSelectedSubject (subject) {
        this.selectedSubject = subject
      }
    }
  })
