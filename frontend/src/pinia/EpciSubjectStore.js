// EpciSubjectStore.js

import { defineStore } from 'pinia'

export const useEpciSubjectStore = defineStore(
  'epciSubject', {
    state: () => ({
      subjects: [
        {
          id: 1,
          name: 'amenagement',
          label: 'Aménagement',
          subjects: [
            { id: 1, name: 'epciBati', label: 'L\'aspect du bâti' },
            { id: 2, name: 'epciStationnement', label: 'Les emplacements de stationnement' }
          ]
        },
        {
          id: 2,
          name: 'carte',
          label: 'Carte',
          subjects: [
            { id: 1, name: 'epciClicMap', label: 'Carte clicable' },
            { id: 2, name: 'epciIGNandSDK', label: 'Orthophoto de l\'IGN' }
          ]
        },
        {
          id: 3,
          name: 'economie',
          label: 'Economie',
          subjects: [
            { id: 1, name: 'epci5G', label: 'Sites mobiles 5G' },
            { id: 2, name: 'epciVoiture', label: 'Equipements des ménages en voitures' },
            { id: 3, name: 'epciFloresNA17', label: 'Activités économiques en 17 postes' },
            { id: 4, name: 'epciSirene', label: 'Liste des entreprises (SIRENE)' }
          ]
        },
        {
          id: 4,
          name: 'environnement',
          label: 'Environnement',
          subjects: [
            { id: 1, name: 'epciRisques', label: 'Les risques' },
            { id: 2, name: 'epciCatNat', label: 'Les catastrophes naturelles' }
          ]
        },
        {
          id: 5,
          name: 'population',
          label: 'Population',
          subjects: [
            { id: 1, name: 'epciPopLeg', label: 'Les populations légales' },
            { id: 2, name: 'epciFamille', label: 'Les types de familles' }
          ]
        },
        {
          id: 6,
          name: 'sport',
          label: 'Sport',
          subjects: [
            { id: 1, name: 'epciBassinNat', label: 'Les bassins de natation' },
            { id: 2, name: 'epciEquipementSport', label: 'Les équipements sportifs à La Réunion' }
          ]
        },
        {
          id: 7,
          name: 'tourisme',
          label: 'Tourisme',
          subjects: [
            { id: 1, name: 'epciEtabTourisme', label: 'Les établissements touristiques' },
            { id: 2, name: 'epciHebTourisme', label: 'Les hebergements touristiques' }
          ]
        }
      ],
      selectedSubject: null
    }),
    actions: {
      setSubjects (subjects) {
        this.subjects = subjects
      },
      setSelectedSubject (subject) {
        this.selectedSubject = subject
      }
    }
  })
