/**
 * Routes epcis.js
 * */

// import PageEPCIDetail from '../../components/pages/PageEPCIDetail'
// import EpciViewPinia from '../../views/EpciViewPinia.vue'
import EpciView from '../../views/EpciView.vue'
// import EpciDynamicSubjectComponent from '../../components/subjects/EpciDynamicSubjectComponent'

// import EpciTheme from '../../components/themes/EpciTheme.vue'
// import EpciData from '../../components/datas/EpciData.vue'

// Data
// Aménagement
import DataEpciBati from '../../components/datas/reunion/amenagement/epcis/DataEpciBati.vue'
import DataEpciStationnement from '../../components/datas/reunion/amenagement/epcis/DataEpciStationnement.vue'

// Carte
import MapEpci from '../../components/maps/reunion/epcis/MapEpci.vue'

// Economie
import DataEpciFloresNA17 from '../../components/datas/reunion/economie/epcis/DataEpciFloresNA17'
import DataEpci5G from '../../components/datas/reunion/economie/epcis/DataEpci5G'
import DataEpciSirene from '../../components/datas/reunion/economie/epcis/DataEpciSirene.vue'
import DataEpciVoiture from '../../components/datas/reunion/economie/epcis/DataEpciVoiture.vue'

// Environnement
// import DataEpciAZI from '../../components/datas/reunion/environnement/epcis/DataEpciAZI'
// import DataEpciCATNAT from '../../components/datas/reunion/environnement/epcis/DataEpciCATNAT'
// import DataEpciMVT from '../../components/datas/reunion/environnement/epcis/DataEpciMVT'
import DataEpciRisques from '../../components/datas/reunion/environnement/epcis/DataEpciRisques'

// Population
import DataEpciPopLeg from '../../components/datas/reunion/population/epcis/DataEpciPopLeg'
import DataEpciFamille from '../../components/datas/reunion/population/epcis/DataEpciFamille'

// Santé
// import DataEpciEtabSante from '../../components/datas/reunion/sante/epcis/DataEpciEtabSante'

// Sport
import DataEpciBassinNat from '../../components/datas/reunion/sport/epcis/DataEpciBassinNat.vue'

// Tourisme
import DataEpciHebTourisme from '../../components/datas/reunion/tourisme/epcis/DataEpciHebTourisme'
import DataEpciEtabTourisme from '../../components/datas/reunion/tourisme/epcis/DataEpciEtabTourisme.vue'

// import MapEpci5G from '../../components/maps/reunion/epcis/MapEpci5G.vue'
const sharedProps = (route) => ({
  paysName: route.params.paysName,
  epciId: parseInt(route.params.epciId),
  epciName: route.params.epciName
})

// epciRouter.js
export default [
  {
    path: '/pays/:paysName/epci/:epciId/:epciName',
    name: 'epciView',
    component: EpciView,
    props: (route) => ({
      ...sharedProps(route),
      themeName: route.params.themeName || 'amenagement' // Thème par défaut
      // selectedScale: route.params.selectedScale || 'epci' // Valeur par défaut
    }),
    beforeEnter: (to, from, next) => {
      console.log('Before entering epci route')
      if (to.params.paysName === 'reunion') {
        next() // Autoriser l'accès si le pays est La Réunion
      } else {
        next({ name: '404' }) // Rediriger vers une route 404 page non trouvée
      }
    },
    // children: [
    //   {
    //     path: ':themeName/:subjectName',
    //     name: 'epciSubject',
    //     component: EpciDynamicSubjectComponent,
    //     props: (route) => ({
    //       ...sharedProps(route),
    //       themeName: route.params.themeName,
    //       subjectName: route.params.subjectName
    //     })
    //   }
    // ]
    children: [
      {
        path: ':themeName/:subjectName',
        name: 'epciBati',
        component: DataEpciBati, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'amenagement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'L\'aspect du bâti' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciStationnement',
        component: DataEpciStationnement, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'amenagement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Emplacements réservés de stationnement' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciMap',
        component: MapEpci, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'carte',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Carte clicable' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epci5G',
        component: DataEpci5G, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Le réseau 5G' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciVoiture',
        component: DataEpciVoiture, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Nombres de voitures par ménage' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciFloresNA17',
        component: DataEpciFloresNA17, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'FLORES - Activité économique en 17 postes' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciSirene',
        component: DataEpciSirene, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'SIRENE - Liste des entreprises' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciRisques',
        component: DataEpciRisques, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'environnement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les risques' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciPopLeg',
        component: DataEpciPopLeg, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'population',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les populations légales' }
      },
      // {
      //   path: 'sante/:subjectName',
      //   name: 'epciEtabSante',
      //   component: DataEpciEtabSante, // Composant pour l'affichage des sujets
      //   props: (route) => ({
      //     ...sharedProps(route),
      //     themeName: themes.amenagement,
      //     subjectName: route.params.subjectName
      //   })
      // },
      {
        path: ':themeName/:subjectName',
        name: 'epciFamille',
        component: DataEpciFamille, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'population',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les bassins de natation' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciBassinNat',
        component: DataEpciBassinNat, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'sport',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les équipements sportifs' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciEtabTourisme',
        component: DataEpciEtabTourisme, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'tourisme',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les établissements touristiques' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'epciHebTourisme',
        component: DataEpciHebTourisme, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'tourisme',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les hébergements touristiques' }
      }
    ]
  }
]
