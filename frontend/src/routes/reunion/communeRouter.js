/**
 * Routes communes.js
 * */

// import CommuneViewPinia from '../../views/CommuneViewPinia.vue'
import CommuneView from '../../views/CommuneView.vue'
// import CommuneDynamicSubjectComponent from '../../components/subjects/ReunionDynamicSubjectComponent'

// import CommuneData from '../../components/datas/CommuneData'
// import CommuneTheme from '../../components/themes/CommuneTheme.vue'

// Datas
// Aménagement
import DataCommuneStationnement from '../../components/datas/reunion/amenagement/communes/DataCommuneStationnement.vue'
import DataCommuneBati from '../../components/datas/reunion/amenagement/communes/DataCommuneBati.vue'
// Carte
// import MapReunionIGNandSDK from '../../components/maps/ign/MapReunionIGNandSDK'
import MapCommune from '../../components/maps/reunion/communes/MapCommune.vue'

// Economie
import DataCommuneFloresNA17 from '../../components/datas/reunion/economie/communes/DataCommuneFloresNA17.vue'
import DataCommune5G from '../../components/datas/reunion/economie/communes/DataCommune5G.vue'
import DataCommuneSirene from '../../components/datas/reunion/economie/communes/DataCommuneSirene.vue'
import DataCommuneVoiture from '../../components/datas/reunion/economie/communes/DataCommuneVoiture.vue'
// Environnement
import DataCommuneRisques from '../../components/datas/reunion/environnement/communes/DataCommuneRisques.vue'
// import DataCommuneAZI from '../../components/datas/reunion/environnement/communes/DataCommuneAZI.vue'
// import DataCommuneCATNAT from '../../components/datas/reunion/environnement/communes/DataCommuneCATNAT.vue'
// import DataCommuneMVT from '../../components/datas/reunion/environnement/communes/DataCommuneMVT.vue'
// Population
import DataCommunePopLeg from '../../components/datas/reunion/population/communes/DataCommunePopLeg.vue'
import DataCommuneFamille from '../../components/datas/reunion/population/communes/DataCommuneFamille.vue'
// Santé
// import DataCommuneEtabSante from '../../components/datas/reunion/sante/communes/DataCommuneEtabSante.vue'
// Sport
import DataCommuneBassinNat from '../../components/datas/reunion/sport/communes/DataCommuneBassinNat.vue'
// Tourisme
import DataCommuneHebTourisme from '../../components/datas/reunion/tourisme/communes/DataCommuneHebTourisme.vue'
import DataCommuneEtabTourisme from '../../components/datas/reunion/tourisme/communes/DataCommuneEtabTourisme.vue'

// communesRouter.js

const sharedProps = (route) => ({
  paysName: route.params.paysName,
  epciId: parseInt(route.params.epciId),
  epciName: route.params.epciName,
  communeId: parseInt(route.params.communeId),
  communeName: route.params.communeName
})

export default [
  {
    path: '/pays/:paysName/epci/:epciId/:epciName/commune/:communeId/:communeName',
    name: 'communeView',
    component: CommuneView,
    props: (route) => ({
      ...sharedProps(route)
      // selectedScale: route.params.selectedScale || 'commune' // Valeur par défaut
    }),
    beforeEnter: (to, from, next) => {
      console.log('Berfore entering commune route')
      if (to.params.paysName === 'reunion') {
        next() // Autoriser l'accès si le pays est La Réunion
      } else {
        next({ name: '404' }) // Rediriger vers une route 404 page non trouvée
      }
    },
    // children: [
    //   {
    //     path: ':themeName/:subjectName',
    //     name: 'communeSubject',
    //     component: CommuneDynamicSubjectComponent,
    //     props: (route) => ({
    //       ...sharedProps(route),
    //       themeName: route.params.themeName,
    //       subjectName: route.params.subjectName
    //     })
    //   }
    // ]

    children: [
      {
        path: ':themeName/:subjectName',
        name: 'communeBati',
        component: DataCommuneBati, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'amenagement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'L\'aspect du bâti' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeStationnement',
        component: DataCommuneStationnement, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'amenagement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Emplacements réservés de stationnement' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeMap',
        component: MapCommune, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'carte',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Carte clicable' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'commune5G',
        component: DataCommune5G, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Le réseau 5G' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeVoiture',
        component: DataCommuneVoiture, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Nombres de voitures par ménage' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeFloresNA17',
        component: DataCommuneFloresNA17, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'FLORES - Activité économique en 17 postes' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeSirene',
        component: DataCommuneSirene, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'SIRENE - Liste des entreprises ' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeRisques',
        component: DataCommuneRisques, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'environnement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les risques' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communePopLeg',
        component: DataCommunePopLeg, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'population',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les populations légales à La Réunion' }
      },
      // {
      //   path: 'sante/:subjectName',
      //   name: 'communeEtabSante',
      //   component: DataCommuneEtabSante, // Composant pour l'affichage des sujets
      //   props: (route) => ({
      //     ...sharedProps(route),
      //     themeName: 'sante',
      //     subjectName: route.params.subjectName
      //   })
      // },

      {
        path: ':themeName/:subjectName',
        name: 'communeFamille',
        component: DataCommuneFamille, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'population',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les populations légales' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeBassinNat',
        component: DataCommuneBassinNat, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'sport',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les bassins de natation' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeEtabTourisme',
        component: DataCommuneEtabTourisme, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'tourisme',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les établissements touristiques à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'communeHebTourisme',
        component: DataCommuneHebTourisme, // Composant pour l'affichage des sujets
        props: (route) => ({
          ...sharedProps(route),
          themeName: 'tourisme',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les hébergements touristiques à La Réunion' }
      }
    ]
  }
]
