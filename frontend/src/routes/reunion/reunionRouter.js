/**
 * Routes reunion.js
 * */

// import PageReunionDetail from '../../components/pages/PageReunionDetail'
// import ReunionViewPinia from '../../views/ReunionViewPinia.vue'
import ReunionView from '../../views/ReunionView.vue'
// import ReunionDynamicSubjectComponent from '../../components/subjects/ReunionDynamicSubjectComponent'

// Datas
// Aménagement
import DataReunionBati from '../../components/datas/reunion/amenagement/region/DataReunionBati'
import DataReunionStationnement from '../../components/datas/reunion/amenagement/region/DataReunionStationnement'
// Carte
import MapReunionClicMap from '../../components/maps/reunion/region/MapReunionClicMap.vue'
import MapReunionIGNandSDK from '../../components/maps/ign/MapReunionIGNandSDK.vue'
// Economie
import DataReunion5G from '../../components/datas/reunion/economie/region/DataReunion5G'
import DataReunionVoiture from '../../components/datas/reunion/economie/region/DataReunionVoiture'
import DataReunionFloresNA17 from '../../components/datas/reunion/economie/region/DataReunionFloresNA17'
import DataReunionSirene from '../../components/datas/reunion/economie/region/DataReunionSirene'
// Environnement
import DataReunionRisques from '../../components/datas/reunion/environnement/region/DataReunionRisques'
// import DataReunionAZI from '../../components/datas/reunion/environnement/region/DataReunionAZI'
import DataReunionCATNAT from '../../components/datas/reunion/environnement/region/DataReunionCATNAT'
// import DataReunionMVT from '../../components/datas/reunion/environnement/region/DataReunionMVT'
// Santé
// import DataReunionEtabSante from '../../components/datas/reunion/sante/region/DataReunionEtabSante'
// Population
import DataReunionPopLeg from '../../components/datas/reunion/population/region/DataReunionPopLeg'
import DataReunionFamille from '../../components/datas/reunion/population/region/DataReunionFamille'
// Sport
import MapReunionBassinNat from '../../components/datas/reunion/sport/region/MapReunionBassinNat'
import DataReunionEquipementsSport from '../../components/datas/reunion/sport/region/DataReunionEquipementsSport'
// Tourisme
import DataReunionEtabTourisme from '../../components/datas/reunion/tourisme/region/DataReunionEtabTourisme'
import DataReunionHebTourisme from '../../components/datas/reunion/tourisme/region/DataReunionHebTourisme'

// reunionRouter.js
export default [
  {
    path: '/pays/:paysName',
    name: 'reunionView',
    component: ReunionView,
    // props: true,
    props: (route) => ({
      paysName: route.params.paysName
    }),
    children: [
      {
        path: ':themeName/:subjectName',
        name: 'reunionBati',
        component: DataReunionBati, // Composant pour l'affichage des sujets
        // props: true
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'amenagement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'L\'aspect du bâti' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionStationnement',
        component: DataReunionStationnement, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'amenagement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Emplacements réservés de stationnement' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionClicMap',
        component: MapReunionClicMap, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'carte',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Carte clicable' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionIGNandSDK',
        component: MapReunionIGNandSDK, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'carte',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Photographie aérienne de La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunion5G',
        component: DataReunion5G, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Le réseau 5G à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionVoiture',
        component: DataReunionVoiture, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Nombres de voitures par ménage à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionFloresNA17',
        component: DataReunionFloresNA17, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'FLORES - Activité économique en 17 postes' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionSirene',
        component: DataReunionSirene, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'economie',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'SIRENE - Liste des entreprises à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionRisques',
        component: DataReunionRisques, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'environnement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les risques à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionCatnat',
        component: DataReunionCATNAT, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'environnement',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les catastrophes naturelles à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionPopLeg',
        component: DataReunionPopLeg, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'population',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les populations légales à La Réunion' }
      },
      // {
      //   path: 'sante/:subjectName',
      //   name: 'reunionEtabSante',
      //   component: DataReunionEtabSante, // Composant pour l'affichage des sujets
      //   props: (route) => ({
      //     paysName: route.params.paysName,
      //     themeName: themes.amenagement, // Thème par défaut
      //     subjectName: route.params.subjectName
      //   })
      // },
      {
        path: ':themeName/:subjectName',
        name: 'reunionFamille',
        component: DataReunionFamille, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'population',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les types de famille à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionBassinNat',
        component: MapReunionBassinNat, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'sport', // Thème par défaut
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les bassins de natation à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionEquipementsSport',
        component: DataReunionEquipementsSport, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'sport',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les équipements sportifs à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionEtabTourisme',
        component: DataReunionEtabTourisme, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'tourisme',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les établissements touristiques à La Réunion' }
      },
      {
        path: ':themeName/:subjectName',
        name: 'reunionHebTourisme',
        component: DataReunionHebTourisme, // Composant pour l'affichage des sujets
        props: (route) => ({
          paysName: route.params.paysName,
          themeName: 'tourisme',
          subjectName: route.params.subjectName
        }),
        meta: { subjectLabel: 'Les hébergements touristiques à La Réunion' }
      }
    ]
  }
]
