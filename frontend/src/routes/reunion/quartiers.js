import PageQuartierDetail from '../../components/pages/PageQuartierDetail'

export default [
  {
    path: '/pays/:paysName/epci/:epciId/:epciName/commune/:communeId/:communeName/quartier/:quartierName',
    name: 'quartier',
    component: PageQuartierDetail,
    props: (route) => ({
      paysName: route.params.paysName,
      epciId: parseInt(route.params.epciId),
      epciName: route.params.epciName,
      communeId: parseInt(route.params.communeId),
      communeName: route.params.communeName,
      quartierName: route.params.quartierName,
      quartierTheme: route.params.quartierTheme
    }),
    beforeEnter: (to, from, next) => {
      if (to.params.paysName === 'reunion') {
        next() // Autoriser l'accès si le pays est La Réunion
      } else {
        next({ name: '404' }) // Rediriger vers une route 404 page non trouvée
      }
    }
  }
]
