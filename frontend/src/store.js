// store.js
import { createStore } from 'vuex'
import communeModule from './modules/communeModule.js'
import epciModule from './modules/epciModule.js'
import reunionModule from './modules/reunionModule.js'

export default createStore({
  modules: {
    reunion: reunionModule,
    epci: epciModule,
    commune: communeModule
  },
  state: {
    showEpciLinks: {
      // Initialisation des données
      CASUD: false,
      CINOR: false,
      CIREST: false,
      CIVIS: false,
      TCO: false
    }
  },
  mutations: {
    // Mutations
    toggleEpciLink (state, elementName) {
      // Inverser la valeur de showEpciLinks pour l'élément spécifié
      state.showEpciLinks[elementName] = !state.showEpciLinks[elementName]
    }
  },
  actions: {
    // Actions
  },
  getters: {
    // Getters
    showEpciLinks (state) {
      return state.showEpciLinks
    }
  }
})
