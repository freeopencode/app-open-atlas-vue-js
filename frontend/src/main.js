// main.js
import { createApp } from 'vue'
import App from './App.vue'
/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'
/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
/* import specific icons */
import { faChevronDown, faChevronRight, faHouse, faBars, faEarthAfrica } from '@fortawesome/free-solid-svg-icons'
import { faYoutube, faMastodon, faFacebook, faGitlab, faXTwitter } from '@fortawesome/free-brands-svg-icons'
import router from './router.js'
import 'bootstrap/dist/css/bootstrap.min.css'
// import store from './store.js'
// import { createPinia } from './pinia.js'
// import { useReunionSubjectStore } from './pinia/ReunionSubjectStore'
// import { useEpciSubjectStore } from './pinia/EpciSubjectStore'
// import { useCommuneSubjectStore } from './pinia/CommuneSubjectStore'

import process from 'process'
window.process = process

/**
 * *************************Initialisation et confguration*****************************
 * */

/* add icons to the library */
library.add(faChevronDown, faChevronRight, faHouse, faBars, faEarthAfrica, faYoutube, faMastodon, faFacebook, faGitlab, faXTwitter)

/**
 * ***************************************Routes*****************************************
 * */

const app = createApp(App)
// const pinia = createPinia()

// Utilisation des variables d'environnement
require('dotenv').config()

app.component('FontAwesomeIcon', FontAwesomeIcon)

// Utiliser Vue Router
app.use(router)

// Utiliser Pinia
// app.use(pinia)

// const reunionSubjectStore = useReunionSubjectStore(pinia)
// reunionSubjectStore.setSubjects(reunionSubjectStore.subjects)

// const epciSubjectStore = useEpciSubjectStore(pinia)
// epciSubjectStore.setSubjects(epciSubjectStore.subjects)

// const communeSubjectStore = useCommuneSubjectStore(pinia)
// communeSubjectStore.setSubjects(communeSubjectStore.subjects)

app.mount('#app')
