const { defineConfig } = require('@vue/cli-service')
const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = defineConfig({
  transpileDependencies: ['vue-meta'],
  configureWebpack: {
    ...require('./webpack.config.js')
  },
  devServer: {
    // Port du serveur frontend
    port: 8081,
    proxy: {
      '/api': {
        target: 'http://localhost:3000', // URL du serveur backend
        changeOrigin: true,
        pathRewrite: {
          '^/api': '' // Retirez "/api" de l'URL
        }
      }
    }
  }
})
