const webpack = require('webpack')
const dotenv = require('dotenv')

module.exports = {
  // Configuration des résolutions
  resolve: {
    fallback: {
      timers: false,
      stream: false,
      crypto: false,
      os: false
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      // Indique qu'on est en environnement client
      'process.env.NODE_ENV': JSON.stringify('client'),
      'process.env': dotenv.parsed
    })
  ]
}
